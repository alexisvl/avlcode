#!/usr/bin/python3
# Copyright (C) 2021 Alexis Lockwood, <alexlockwood@fastmail.com>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.

import os
import shlex
import subprocess
import sys
import textwrap

from colorama import Fore, Style

VERBOSE = os.getenv("VERBOSE") not in (None, "0", "no")
MAXCOLS = 120
DOT = "·"

if sys.argv[1].startswith("-c"):
    COLS = int(sys.argv[1][2:])
    sys.argv = [sys.argv[0], *sys.argv[2:]]
else:
    COLS = os.get_terminal_size().columns

if COLS > MAXCOLS:
    COLS = MAXCOLS

TAG_WIDTH = 8
DEFAULT_TAG_COLOR = Fore.YELLOW
WRAPPER = textwrap.TextWrapper(
    width = COLS - TAG_WIDTH - 2,
    replace_whitespace = False,
    drop_whitespace = False,
)

CMD = sys.argv[1] if len(sys.argv) > 1 else None

def tag_to_color(tag):
    """Decode a tag in the format ":color:tag" to (color, tag), returning
    the default color otherwise"""

    if tag.startswith(":"):
        color, delim, tag = tag[1:].partition(":")
        color_cmd = {
            "black": Fore.BLACK,
            "red": Fore.RED,
            "green": Fore.GREEN,
            "yellow": Fore.YELLOW,
            "blue": Fore.BLUE,
            "magenta": Fore.MAGENTA,
            "cyan": Fore.CYAN,
            "white": Fore.WHITE,
            "reset": Fore.RESET,
        }.get(color, DEFAULT_TAG_COLOR)
        return color_cmd, tag
    else:
        return DEFAULT_TAG_COLOR, tag

def print_nice(text):
    #text_lines = [WRAPPER.fill(i) for i in text.split("\n")]
    for i in text.split("\n"):
        print(" " * TAG_WIDTH, i)

def do_info(tag, text, color=Fore.YELLOW):
    text_lines = WRAPPER.wrap(text)
    tag_col = tag[:TAG_WIDTH]
    tag_col += (TAG_WIDTH - len(tag_col)) * DOT

    print(color + tag_col + Style.RESET_ALL, text_lines[0])
    for i in text_lines[1:]:
        print(" " * TAG_WIDTH, i)

def progressbar(value, total, suffix):
    # #####....... 9999/9999 kB, 100%
    label_len = 5 + 1 + 5 + 1 + len(suffix) + 2 + 8 + 1
    pct = 100 * value / total
    label = f"{round(value):5}/{round(total):5} {suffix}, {round(pct,1):5}%"
    length = COLS - TAG_WIDTH - 4 - label_len
    length_shown = int(round(value * length / total))
    #return label + "  [" + ("▒" * length_shown) + ("═" * (length - length_shown)) + "]"
    return label + "  [" + ("═" * length_shown) + ("┈" * (length - length_shown)) + "]"

if CMD == "info":
    color, tag = tag_to_color(sys.argv[2])
    text = " ".join(sys.argv[3:])
    do_info(tag, text, color=color)

elif CMD == "run":
    color, tag = tag_to_color(sys.argv[2])
    sep = sys.argv.index("--")
    text = sys.argv[3:sep]
    cmd = sys.argv[sep+1:]
    cmd_str = " ".join(shlex.quote(i) for i in cmd)

    do_info(tag, " ".join(text), color=color)
    if VERBOSE:
        do_info("$", cmd_str, color=color)

    cp = subprocess.run(
        cmd,
        stderr=subprocess.STDOUT,
        stdout=subprocess.PIPE,
        text=True,
    )
    if cp.returncode:
        if not VERBOSE:
            do_info(DOT + "FAILED", cmd_str, color=Fore.RED)
            print()
        print_nice(cp.stdout)
        sys.exit(cp.returncode)

elif CMD == "size":
    fn = sys.argv[2]
    mcu = sys.argv[3]
    flashsz = int(sys.argv[4])
    fdatsz = int(sys.argv[5])
    ramsz = int(sys.argv[6])
    eesz = int(sys.argv[7])

    cmd = ["avr-size", "-A", "--", fn]
    cp = subprocess.run(
        cmd,
        stderr=subprocess.STDOUT,
        stdout=subprocess.PIPE,
        text=True,
    )
    if cp.returncode:
        do_info(".FAILED", " ".join(shlex.quote(i) for i in cmd), color=Fore.RED)
        print()
        print_nice(cp.stdout)
        sys.exit(cp.returncode)
    else:
        table = [i.split() for i in cp.stdout.split("\n")]
        table_dict = {}
        for sec, size, addr in [i for i in table if len(i) == 3]:
            try:
                table_dict[sec] = (int(size), int(addr))
            except ValueError:
                pass

        s_text = table_dict.get(".text", (0, 0))[0]
        s_data = table_dict.get(".data", (0, 0))[0]
        s_bss = table_dict.get(".bss", (0, 0))[0]
        s_fdat = table_dict.get(".fdat", (0, 0))[0]
        s_eeprom = table_dict.get(".eeprom", (0, 0))[0]

        progmem = s_text + s_data + s_fdat
        fdat = s_fdat
        sram = s_data + s_bss
        eeprom = s_eeprom

        do_info("FLASH", progressbar(progmem/1024, flashsz/1024, "kB"))
        do_info("(data)", progressbar(s_data/1024, flashsz/1024, "kB"))
        if fdatsz:
            do_info("(fdat)", progressbar(fdat/1024, fdatsz/1024, "kB"))
        do_info("SRAM", progressbar(sram/1024, ramsz/1024, "kB"))
        do_info("EEPROM", progressbar(eeprom, eesz, " B"))

elif CMD == "hbar":
    print("─" * COLS)

else:
    print(f"usage: {sys.argv[0]} * info TAG TEXT")
    print( "            - print TEXT with prefix TAG")
    print(f"       {sys.argv[0]} * run TAG TEXT -- CMD")
    print( "            - run CMD, printing TAG/TEXT, and the full command on error")
    print(f"       {sys.argv[0]} * size ELF MCU FLASHSZ FDATSZ RAMSZ EESZ")
    print( "            - custom avr-size with arbitrary memory sizes")
    print(f"       {sys.argv[0]} * hbar")
    print( "            - fill the screen with a line")
    print()
    print( " * can be empty, or -cN to set terminal width to N")
    print( " Tags can optionally be formatted \":color:tag\"")
