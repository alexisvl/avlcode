// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef HARDWARE_HPP
#define HARDWARE_HPP

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include "avl_avrdx.hpp"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------
// --- PUBLIC CONSTANTS --------------------------------------------------------

static avl::usart<avl::USART2_ADDR>             amib_usart2;
static avl::pin_group<avl::PORTD_ADDR, 0xFF>   amib_debug_port;

// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

#endif // !defined(HARDWARE_HPP)
