// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef AVL_QUEUE_HPP
#define AVL_QUEUE_HPP

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include "avl_error.hpp"
#include "avl_types.hpp"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

namespace avl {

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------

/// Simple FIFO queue. Threadsafe for single producer and single consumer ONLY,
/// and supports 255 elements max.
template<typename T, uint8_t length>
class queue {
    static_assert(length >= 2, "length must be at least 2");
    static_assert(length <= 255, "length must be 255 or less");

    uint8_t volatile m_head;
    uint8_t volatile m_tail;
    T m_data[length];

public:
    /// Construct a queue
    constexpr queue() : m_head(0), m_tail(0) {}

    /// Return whether the queue is empty
    bool empty() const { return m_head == m_tail; }

    /// Return whether the queue is full
    bool full() const { return weight() >= length - 1; }

    /// Return the number of elements currently in the queue
    uint8_t weight() const {
        uint8_t const head = m_head, tail = m_tail;

        if (head >= tail) {
            return head - tail;
        } else {
            return (length - tail) + head;
        }
    }

    /// Return a pointer to the next element to be read. Does not pop.
    ///
    /// @retval error_t::NOSPC - queue is empty
    result_t<T const *> read() const {
        __sync_synchronize();
        if (m_head == m_tail) {
            return error_t::NOSPC;
        } else {
            return &m_data[m_tail];
        }
    }

    /// Pop an item off the queue.
    ///
    /// @param dest - buffer to hold item. May be null if the item content is
    ///     not needed.
    /// @retval error_t::OK - an item was popped
    /// @retval error_t::NOENT - queue empty
    error_t pop(T * dest) {
        result_t<T const *> item = read();

        if (item.is_ok()) {
            if (dest) {
                *dest = *item.ok();
            }

            uint8_t new_tail = m_tail + 1;
            if (new_tail >= length) {
                new_tail = 0;
            }
            m_tail = new_tail;
            __sync_synchronize();
            return error_t::OK;
        } else {
            return error_t::NOENT;
        }
    }

    /// Pop an item off the queue, returning it by value.
    ///
    /// @return popped item
    /// @retval error_t::NOENT - queue empty
    result_t<T> pop() {
        T t;
        error_t e = pop(&t);

        if (e == error_t::OK) {
            return t;
        } else {
            return error_t::NOENT;
        }
    }

    /// Return a writable pointer to construct an item in the queue in-place.
    /// Item must be finalized with push_emplaced() before it is available, and
    /// two calls to this function with no push_emplaced() betweeen them will
    /// return the same pointer.
    ///
    /// @retval result_t(ptr) - ptr is space to write into
    /// @retval result_t(error_t::NOSPC) - queue is full
    result_t<T *> emplace_ptr() {
        __sync_synchronize();
        if (full()) {
            return error_t::NOSPC;
        } else {
            return &m_data[m_head];
        }
    }

    /// Make the last emplate_ptr() available for read and pop. If emplace_ptr()
    /// has not been called, this will result in uninitialized data being pushed.
    ///
    /// @retval error_t::OK - success
    /// @retval error_t::ENOSPC - no space in queue. This will only happen if
    ///     emplace_ptr() was not called or did not return a pointe.r
    error_t push_emplaced() {
        result_t<T *> item = emplace_ptr();

        if (item.is_ok()) {
            uint8_t new_head = m_head + 1;
            if (new_head >= length) {
                new_head = 0;
            }
            m_head = new_head;
            __sync_synchronize();
            return error_t::OK;
        } else {
            return error_t::NOSPC;
        }
    }
};

// --- PUBLIC CONSTANTS --------------------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

} // avl

#endif // !defined(AVL_QUEUE_HPP)
