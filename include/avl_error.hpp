// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef AVL_ERROR_HPP
#define AVL_ERROR_HPP

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include "avl_types.hpp"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

namespace avl {

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------

enum class error_t : uint8_t {
    OK = 0,

    PERM = 1,
    NOENT = 2,
    INTR = 4,
    IO = 5,
    NXIO = 6,
    TOOBIG = 7,
    AGAIN = 11,
    NOMEM = 12,
    ACCES = 13,
    FAULT = 14,
    BUSY = 16,
    EXIST = 17,
    NODEV = 19,
    INVAL = 22,
    NOSPC = 28,
    DOM = 33,
    RANGE = 34,
    NOSYS = 38,
    OVERFLOW = 75,
    MSGSIZE = 90,
    TIMEDOUT = 116,
    NOTINIT = 254,
    UNKNOWN = 255,
};

/// Rust-like result class. Can contain either an error or a return value.
template<typename T> class result_t {
    error_t m_e;
    union {
        T m_t;
        int m_placeholder; ///< shut up about uninitialized m_t
    };

public:
    constexpr result_t(T&& t) : m_e(error_t::OK), m_t(t) {}
    constexpr result_t(error_t e) : m_e(e), m_placeholder(0) {}
    constexpr result_t(result_t<T> const &) = default;

    /// Returns true if the result_t contains a successful result
    bool is_ok() const { return m_e == error_t::OK; }

    /// Returns true if the result_t contains an error
    bool is_err() const { return m_e != error_t::OK; }

    /// Access the contained value if there is one. Undefined if is_err().
    T ok() const { return m_t; }

    /// Access the contained value if there is one; return other if is_err().
    T ok_or(T other) const { return is_ok() ? m_t : other; }

    /// Return the error code (error_t::OK if success)
    error_t err() const { return m_e; }

    /// Compare against the contained value, returning false if there isn't one
    bool operator==(T const & other) const { return is_ok() ? (m_t == other) : false; }

    /// Compare against the contained value, returning true if there isn't one
    bool operator!=(T const & other) const { return is_ok() ? (m_t != other) : true; }

    /// Compare against the contained error, returning false if there isn't one
    bool operator==(error_t other) const { return is_err() ? (m_e == other) : false; }

    /// Compare against the contained value, returning true if there isn't one
    bool operator!=(error_t other) const { return is_err() ? (m_e != other) : true; }

    /// Assign the result
    result_t<T>& operator=(result_t<T> const & other) {
        m_e = other.err();
        if (other.is_ok()) {
            m_t = other.ok();
        }
        return *this;
    }

    /// Assign the result
    result_t<T>& operator=(T const & other) {
        m_e = error_t::OK;
        m_t = other;
        return *this;
    }

    /// Assign the result
    result_t<T>& operator=(error_t const & other) {
        m_e = other;
        return *this;
    }
};

// --- PUBLIC CONSTANTS --------------------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

/// Return the short name of an error
charstring_t error_name(error_t e);

/// Return a full description for the error
charstring_t error_describe(error_t e);

/// Convert a uC/OS-II error code to an error_t.
error_t error_from_ucos(uint8_t e);

} // avl

#endif // !defined(AVL_ERROR_HPP)
