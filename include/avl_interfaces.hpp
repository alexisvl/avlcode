// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef AVL_INTERFACES_HPP
#define AVL_INTERFACES_HPP

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include "avl_error.hpp"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>

namespace avl {

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------

/// Byte output stream interface. This interface accepts bytes and emits them
/// via some transmitter.
struct byte_ostream {
    byte_ostream()
    {
#ifdef __AVR__
        m_f = (FILE) {
            .buf = NULL,
            .unget = 0,
            .flags = _FDEV_SETUP_WRITE,
            .size = 0,
            .len = 0,
            .put = _put,
            .get = NULL,
            .udata = static_cast<void*>(this),
        };
#endif // __AVR__
    }

    /// Send one byte
    virtual void send(uint8_t b) = 0;

    /// Send many bytes
    virtual void send(uint8_t const * data, size_t len) {
        for (size_t i = 0; i < len; i += 1) {
            send(data[i]);
        }
    }

    void send_str(char const * data, size_t len) {
        send((uint8_t const *) data, len);
    }

    void send_str(char const * data) {
        send((uint8_t const *) data, strlen(data));
    }

#ifdef __AVR__
    /// AVR only: print formatted text to this interface
    ///
    /// @return number of bytes printed
    size_t printf_P(char const * fmt, ...)
    {
        va_list ap;
        va_start(ap, fmt);
        int n = vfprintf_P(&m_f, fmt, ap);
        return n < 0 ? 0 : n;
    }

    static int _put(char c, FILE * f) {
        byte_ostream *os = static_cast<byte_ostream *>(f->udata);
        os->send((uint8_t) c);
        return c;
    }
private:
    FILE m_f;
#endif // __AVR__
};

/// I2C stream interface.
struct i2c_stream {
    /// Standard packet transaction. First `buffer_out` is sent in write mode,
    /// then `buffer_in` is received.
    ///
    /// @param addr - I2C device address, right-aligned.
    /// @param buffer_out - Buffer to transmit. May be null if out_len is 0.
    /// @param out_len - Number of bytes to send. May be 0.
    /// @param buffer_in - Buffer to receive. May be the same as buffer_out. May
    ///     be null if in_len is 0.
    /// @param in_len - Number of bytes to receive. May be 0.
    ///
    /// @retval error_t::OK - success
    /// @retval error_t::NODEV - device did not ack its address
    /// @retval error_t::IO - device nacked during transmission
    virtual error_t txrx(
        uint8_t addr,
        uint8_t const * buffer_out,
        uint8_t out_len,
        uint8_t * buffer_in,
        uint8_t in_len
    ) = 0;

    /// Transmit only.
    ///
    /// @param addr - I2C device address, right-aligned.
    /// @param buffer_out - Buffer to transmit. May be null if out_len is 0.
    /// @param out_len - Number of bytes to send. May be 0.
    error_t tx(uint8_t addr, uint8_t const * buffer_out, uint8_t out_len) {
        return txrx(addr, buffer_out, out_len, nullptr, 0);
    }
};

// --- PUBLIC CONSTANTS --------------------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

} // avl

#endif // !defined(AVL_INTERFACES_HPP)
