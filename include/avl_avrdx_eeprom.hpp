// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef AVL_AVRDX_EEPROM_HPP
#define AVL_AVRDX_EEPROM_HPP

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

namespace avl {

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------
// --- PUBLIC CONSTANTS --------------------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

/// EEPROM update function compatible with avr-libc (which doesn't seem to
/// support AVR-DX yet)
///
/// @param src - source address
/// @param dest - destination address
/// @param n - number of bytes
void avrdx_eeprom_update_block(void const * src, void * dest, size_t n);

} // avl
#endif // !defined(AVL_AVRDX_EEPROM_HPP)
