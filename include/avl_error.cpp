// Copyright (C) 2021 Alexis Lockwood, <alexlockwood@fastmail.com>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

// --- DEPENDENCIES ------------------------------------------------------------

// This module
#include "avl_error.hpp"

// Supporting modules

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <avr/pgmspace.h>

using namespace avl;

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

charstring_t avl::error_name(error_t e)
{
    switch ((int) e) {
    case (int) error_t::OK:       return CHARSTRING("OK");
    case (int) error_t::PERM:     return CHARSTRING("EPERM");
    case (int) error_t::NOENT:    return CHARSTRING("ENOENT");
    case (int) error_t::INTR:     return CHARSTRING("EINTR");
    case (int) error_t::IO:       return CHARSTRING("EIO");
    case (int) error_t::NXIO:     return CHARSTRING("ENXIO");
    case (int) error_t::TOOBIG:   return CHARSTRING("ETOOBIG");
    case (int) error_t::AGAIN:    return CHARSTRING("EAGAIN");
    case (int) error_t::NOMEM:    return CHARSTRING("ENOMEM");
    case (int) error_t::ACCES:    return CHARSTRING("EACCES");
    case (int) error_t::FAULT:    return CHARSTRING("EFAULT");
    case (int) error_t::BUSY:     return CHARSTRING("EBUSY");
    case (int) error_t::EXIST:    return CHARSTRING("EEXIST");
    case (int) error_t::NODEV:    return CHARSTRING("ENODEV");
    case (int) error_t::INVAL:    return CHARSTRING("EINVAL");
    case (int) error_t::NOSPC:    return CHARSTRING("ENOSPC");
    case (int) error_t::DOM:      return CHARSTRING("EDOM");
    case (int) error_t::RANGE:    return CHARSTRING("ERANGE");
    case (int) error_t::NOSYS:    return CHARSTRING("ENOSYS");
    case (int) error_t::OVERFLOW: return CHARSTRING("EOVERFLOW");
    case (int) error_t::MSGSIZE:  return CHARSTRING("EMSGSIZE");
    case (int) error_t::TIMEDOUT: return CHARSTRING("ETIMEDOUT");
    case (int) error_t::NOTINIT:  return CHARSTRING("ENOTINIT");
    default:                return CHARSTRING("E(unknown)");
    }
}

charstring_t avl::error_describe(error_t e)
{
    switch ((int) e) {
    case (int) error_t::OK:       return CHARSTRING("OK");
    case (int) error_t::PERM:     return CHARSTRING("Operation not permitted");
    case (int) error_t::NOENT:    return CHARSTRING("No such entry");
    case (int) error_t::INTR:     return CHARSTRING("Interrupted call");
    case (int) error_t::IO:       return CHARSTRING("I/O error");
    case (int) error_t::NXIO:     return CHARSTRING("No such device or address");
    case (int) error_t::TOOBIG:   return CHARSTRING("Argument list too long");
    case (int) error_t::AGAIN:    return CHARSTRING("Try again");
    case (int) error_t::NOMEM:    return CHARSTRING("Out of memory");
    case (int) error_t::ACCES:    return CHARSTRING("Permission denied");
    case (int) error_t::FAULT:    return CHARSTRING("Bad address");
    case (int) error_t::BUSY:     return CHARSTRING("Resource busy");
    case (int) error_t::EXIST:    return CHARSTRING("Already exists");
    case (int) error_t::NODEV:    return CHARSTRING("No such device");
    case (int) error_t::INVAL:    return CHARSTRING("Invalid argument");
    case (int) error_t::NOSPC:    return CHARSTRING("No space left");
    case (int) error_t::DOM:      return CHARSTRING("Domain error");
    case (int) error_t::RANGE:    return CHARSTRING("Range error");
    case (int) error_t::NOSYS:    return CHARSTRING("Not implemented");
    case (int) error_t::OVERFLOW: return CHARSTRING("Overflow error");
    case (int) error_t::MSGSIZE:  return CHARSTRING("Message too long");
    case (int) error_t::TIMEDOUT: return CHARSTRING("Timed out");
    case (int) error_t::NOTINIT:  return CHARSTRING("Not initialized");
    default:                return CHARSTRING("Unknown error");
    }
}

error_t avl::error_from_ucos(uint8_t e)
{
    static const PROGMEM error_t lookup[] = {
        error_t::OK,        //   0 = OS_ERR_NONE
        error_t::INVAL,     //   1 = OS_ERR_EVENT_TYPE
        error_t::PERM,      //   2 = OS_ERR_PEND_ISR
        error_t::INVAL,     //   3 = OS_ERR_POST_NULL_PTR
        error_t::INVAL,     //   4 = OS_ERR_PEVENT_NULL
        error_t::PERM,      //   5 = OS_ERR_POST_ISR
        error_t::PERM,      //   6 = OS_ERR_QUERY_ISR
        error_t::INVAL,     //   7 = OS_ERR_INVALID_OPT
        error_t::INVAL,     //   8 = OS_ERR_ID_INVALID
        error_t::INVAL,     //   9 = OS_ERR_PDATA_NULL
        error_t::TIMEDOUT,  //  10 = OS_ERR_TIMEOUT
        error_t::MSGSIZE,   //  11 = OS_ERR_EVENT_NAME_TOO_LONG
        error_t::INVAL,     //  12 = OS_ERR_PNAME_NULL
        error_t::ACCES,     //  13 = OS_ERR_PEND_LOCKED
        error_t::INTR,      //  14 = OS_ERR_PEND_ABORT
        error_t::PERM,      //  15 = OS_ERR_DEL_ISR
        error_t::PERM,      //  16 = OS_ERR_CREATE_ISR
        error_t::PERM,      //  17 = OS_ERR_NAME_GET_ISR
        error_t::PERM,      //  18 = OS_ERR_NAME_SET_ISR
        error_t::ACCES,     //  19 = OS_ERR_ILLEGAL_CREATE_RUN_TIME
        error_t::NOSPC,     //  20 = OS_ERR_MBOX_FULL
        error_t::ACCES,     //  21 = OS_ERR_ILLEGAL_DEL_RUN_TIME
        error_t::UNKNOWN, // 22
        error_t::UNKNOWN, // 23
        error_t::UNKNOWN, // 24
        error_t::UNKNOWN, // 25
        error_t::UNKNOWN, // 26
        error_t::UNKNOWN, // 27
        error_t::UNKNOWN, // 28
        error_t::UNKNOWN, // 29
        error_t::NOSPC,     //  30 = OS_ERR_Q_FULL
        error_t::NOENT,     //  31 = OS_ERR_Q_EMPTY
        error_t::UNKNOWN, // 32
        error_t::UNKNOWN, // 33
        error_t::UNKNOWN, // 34
        error_t::UNKNOWN, // 35
        error_t::UNKNOWN, // 36
        error_t::UNKNOWN, // 37
        error_t::UNKNOWN, // 38
        error_t::UNKNOWN, // 39

        error_t::EXIST,     //  40 = OS_ERR_PRIO_EXIST
        error_t::NOENT,     //  41 = OS_ERR_PRIO (doesn't exist)
        error_t::INVAL,     //  42 = OS_ERR_PRIO_INVALID
        error_t::UNKNOWN, // 43
        error_t::UNKNOWN, // 44
        error_t::UNKNOWN, // 45
        error_t::UNKNOWN, // 46
        error_t::UNKNOWN, // 47
        error_t::UNKNOWN, // 48
        error_t::UNKNOWN, // 49

        error_t::ACCES,     //  50 = OS_ERR_SCHED_LOCKED
        error_t::NOSPC,     //  51 = OS_ERR_SEM_OVF
        error_t::UNKNOWN, // 52
        error_t::UNKNOWN, // 53
        error_t::UNKNOWN, // 54
        error_t::UNKNOWN, // 55
        error_t::UNKNOWN, // 56
        error_t::UNKNOWN, // 57
        error_t::UNKNOWN, // 58
        error_t::UNKNOWN, // 59

        error_t::PERM,      //  60 = OS_ERR_TASK_CREATE_ISR
        error_t::PERM,      //  61 = OS_ERR_TASK_DEL
        error_t::PERM,      //  62 = OS_ERR_TASK_DEL_IDLE
        error_t::UNKNOWN,   //  63 = OS_ERR_TASK_DEL_REQ
        error_t::PERM,      //  64 = OS_ERR_TASK_DEL_ISR
        error_t::MSGSIZE,   //  65 = OS_ERR_TASK_NAME_TOO_LONG
        error_t::NOSPC,     //  66 = OS_ERR_TASK_NO_MORE_TCB
        error_t::NOENT,     //  67 = OS_ERR_TASK_NOT_EXIST
        error_t::EXIST,     //  68 = OS_ERR_TASK_NOT_SUSPEND (already resumed)
        error_t::NOSYS,     //  69 = OS_ERR_TASK_OPT (tried to check stack, not enabled)
        error_t::NOENT,     //  70 = OS_ERR_TASK_RESUME_PRIO (doesn't exist)
        error_t::PERM,      //  71 = OS_ERR_TASK_SUSPEND_IDLE
        error_t::NOENT,     //  72 = OS_ERR_TASK_SUSPEND_PRIO
        error_t::BUSY,      //  73 = OS_ERR_TASK_WAITING
        error_t::UNKNOWN, // 74
        error_t::UNKNOWN, // 75
        error_t::UNKNOWN, // 76
        error_t::UNKNOWN, // 77
        error_t::UNKNOWN, // 78
        error_t::UNKNOWN, // 79

        error_t::BUSY,      //  80 = OS_ERR_TIME_NOT_DLY
        error_t::INVAL,     //  81 = OS_ERR_TIME_INVALID_MINUTES
        error_t::INVAL,     //  82 = OS_ERR_TIME_INVALID_SECONDS
        error_t::INVAL,     //  83 = OS_ERR_TIME_INVALID_MS
        error_t::DOM,       //  84 = OS_ERR_TIME_ZERO_DLY
        error_t::PERM,      //  85 = OS_ERR_TIME_DLY_ISR
        error_t::UNKNOWN, // 86
        error_t::UNKNOWN, // 87
        error_t::UNKNOWN, // 88
        error_t::UNKNOWN, // 89

        error_t::NOSPC,     //  90 = OS_ERR_MEM_INVALID_PART
        error_t::INVAL,     //  91 = OS_ERR_MEM_INVALID_BLKS
        error_t::INVAL,     //  92 = OS_ERR_MEM_INVALID_SIZE
        error_t::NOSPC,     //  93 = OS_ERR_MEM_NO_FREE_BLKS
        error_t::EXIST,     //  94 = OS_ERR_MEM_FULL (double free, roughly)
        error_t::INVAL,     //  95 = OS_ERR_MEM_INVALID_PBLK
        error_t::INVAL,     //  96 = OS_ERR_MEM_INVALID_PMEM
        error_t::INVAL,     //  97 = OS_ERR_MEM_INVALID_PDATA
        error_t::INVAL,     //  98 = OS_ERR_MEM_INVALID_ADDR
        error_t::MSGSIZE,   //  99 = OS_ERR_MEM_NAME_TOO_LONG

        error_t::ACCES,     // 100 = OS_ERR_NOT_MUTEX_OWNER
        error_t::UNKNOWN, // 101
        error_t::UNKNOWN, // 102
        error_t::UNKNOWN, // 103
        error_t::UNKNOWN, // 104
        error_t::UNKNOWN, // 105
        error_t::UNKNOWN, // 106
        error_t::UNKNOWN, // 107
        error_t::UNKNOWN, // 108
        error_t::UNKNOWN, // 109

        error_t::INVAL,     // 110 = OS_ERR_FLAG_INVALID_PGRP
        error_t::INVAL,     // 111 = OS_ERR_FLAG_WAIT_TYPE
        error_t::BUSY,      // 112 = OS_ERR_FLAG_NOT_RDY
        error_t::INVAL,     // 113 = OS_ERR_FLAG_INVALID_OPT
        error_t::NOENT,     // 114 = OS_ERR_FLAG_GRP_DELETED
        error_t::MSGSIZE,   // 115 = OS_ERR_FLAG_NAME_TOO_LONG
        error_t::UNKNOWN, // 116
        error_t::UNKNOWN, // 117
        error_t::UNKNOWN, // 118
        error_t::UNKNOWN, // 119

        error_t::INVAL,     // 120 = OS_ERR_PCP_LOWER
        error_t::UNKNOWN, // 121
        error_t::UNKNOWN, // 122
        error_t::UNKNOWN, // 123
        error_t::UNKNOWN, // 124
        error_t::UNKNOWN, // 125
        error_t::UNKNOWN, // 126
        error_t::UNKNOWN, // 127
        error_t::UNKNOWN, // 128
        error_t::UNKNOWN, // 129

        error_t::INVAL,     // 130 = OS_ERR_TMR_INVALID_DLY
        error_t::INVAL,     // 131 = OS_ERR_TMR_INVALID_PERIOD
        error_t::INVAL,     // 132 = OS_ERR_TMR_INVALID_OPT
        error_t::INVAL,     // 133 = OS_ERR_TMR_INVALID_NAME
        error_t::NODEV,     // 134 = OS_ERR_TMR_NON_AVAIL
        error_t::ACCES,     // 135 = OS_ERR_TMR_INACTIVE
        error_t::INVAL,     // 136 = OS_ERR_TMR_INVALID_DEST
        error_t::INVAL,     // 137 = OS_ERR_TMR_INVALID_TYPE
        error_t::INVAL,     // 138 = OS_ERR_TMR_INVALID
        error_t::PERM,      // 139 = OS_ERR_TMR_ISR
        error_t::MSGSIZE,   // 140 = OS_ERR_TMR_NAME_TOO_LONG
        error_t::INVAL,     // 141 = OS_ERR_TMR_INVALID_STATE
        error_t::ACCES,     // 142 = OS_ERR_TMR_STOPPED
        error_t::INVAL,     // 143 = OS_ERR_TMR_NO_CALLBACK
        error_t::UNKNOWN, // 144
        error_t::UNKNOWN, // 145
        error_t::UNKNOWN, // 146
        error_t::UNKNOWN, // 147
        error_t::UNKNOWN, // 148
        error_t::UNKNOWN, // 149

        error_t::NOSPC,     // 150 = OS_ERR_NO_MORE_ID_AVAIL
        error_t::UNKNOWN, // 151
        error_t::UNKNOWN, // 152
        error_t::UNKNOWN, // 153
        error_t::UNKNOWN, // 154
        error_t::UNKNOWN, // 155
        error_t::UNKNOWN, // 156
        error_t::UNKNOWN, // 157
        error_t::UNKNOWN, // 158
        error_t::UNKNOWN, // 159

        error_t::NOSPC,     // 160 = OS_ERR_TLS_NO_MORE_AVAIL
        error_t::INVAL,     // 161 = OS_ERR_TLS_ID_ENVALID
        error_t::NOSYS,     // 162 = OS_ERR_TLS_NOT_EN
        error_t::EXIST,     // 163 = OS_ERR_TLS_DESTRUCT_ASSIGNED
        error_t::NOTINIT,   // 164 = OS_ERR_OS_NOT_RUNNING
    };

    if (e >= sizeof(lookup)) {
        return error_t::UNKNOWN;
    } else {
        return (error_t) pgm_read_byte(&lookup[e]);
    }
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------
