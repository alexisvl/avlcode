/*
 * fletcher checksum library
 * Copyright (C) 2021  Alexis Lockwood
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FLETCHER16_H
#define FLETCHER16_H 1

#include <inttypes.h>

#ifdef __cplusplus
extern "C" {
#endif

#define FLETCHER16_INIT 0u

/// Compute a fletcher16 checksum. Call on each byte to update the
/// checksum.
///
/// @param previous - previous checksum value, or FLETCHER16_INIT
/// @param data - data byte to update checksum with
///
/// @return fletcher16 checksum
__attribute__((const))
uint16_t fletcher16(uint16_t previous, uint8_t data);

#ifdef __cplusplus
}
#endif

#endif // FLETCHER16_H
