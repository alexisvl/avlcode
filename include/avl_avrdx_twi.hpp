// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef AVL_AVRDX_TWI_HPP
#define AVL_AVRDX_TWI_HPP

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include <avr/io.h>
#include <ucos_ii.h>
#include "avl_types.hpp"
#include "avl_i2c_i.hpp"
#include "avl_avrdx_port.hpp"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

namespace avl {

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC CONSTANTS --------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------

struct avrdx_twi: public i2c_i
{
	struct cfg
	{
		bool fast_mode_plus;
		/// Baud register computation is a bit complicated. Do it by
		/// hand.
		///
		/// Per the datasheet:
		///
		///   Tr = bus rise time
		///   Tof = bus fall time
		///
		/// First,
		///   BAUD = fCLK_PER / (2 fSCL) - (5 + fCLK_PER Tr / 2)
		///
		/// Then, check if that gives a Tlow that means the limits for
		/// the selected operating mode:
		///   Tlow = (BAUD + 5) / fCLK_PER - Tof
		///
		///   Limits: 4.7us (standard), 1.3us (fast), 500ns (fm+)
		///
		/// If not, recompute:
		///   BAUD = fCLK_PER * (Tlow + Tof) - 5
		uint8_t baudreg;
	};

	avrdx_twi(TWI_t * twi);

	/// Initialize GPIOs. Sets up portmux and works around an erratum.
	/// Should be called before init().
	///
	/// @param Sda - type of SDA pin (avl::pin<>)
	/// @param Scl - type of SDA pin (avl::pin<>)
	/// @param puen - whether to enable pullups
	///
	/// @retval error_t::OK - success
	/// @retval error_t::INVAL - invalid pins for peripheral
	template<typename Sda, typename Scl>
	error_t init_pins(bool puen)
	{
		Sda sda;
		Scl scl;
		if (&sda.vport() != &scl.vport())
			return error_t::INVAL;
		if (scl.npin() != 3 || sda.npin() != 2)
			return error_t::INVAL;

		bool const twi1 = (m_twi == &TWI1);
		uint8_t pmux = 0;

		if (!twi1 && &sda.vport() == &VPORTA)
			pmux = PORTMUX_TWI0_DEFAULT_gc;
		else if (!twi1 && &sda.vport() == &VPORTC)
			pmux = PORTMUX_TWI0_ALT2_gc;
		else if (twi1 && &sda.vport() == &VPORTF)
			pmux = PORTMUX_TWI1_DEFAULT_gc;
		else if (twi1 && &sda.vport() == &VPORTB)
			pmux = PORTMUX_TWI1_ALT2_gc;
		else
			return error_t::INVAL;

		PORTMUX.TWIROUTEA =
			(PORTMUX.TWIROUTEA
			    & ~(twi1 ? PORTMUX_TWI1_gm : PORTMUX_TWI0_gm))
			| pmux;

		if (puen)
		{
			sda << port_pinctrl::pullup::yes;
			scl << port_pinctrl::pullup::yes;
		}

		// Erratum 2.9.1: output pins must have OUT=0
		sda = 0;
		scl = 0;

		return error_t::OK;
	}

	/// Initialize. Must be called after OS initialization.
	error_t init(cfg const & config);

	/// Transmit and receive.
	///
	/// @retval error_t::OK - success
	/// @retval error_t::NODEV - address was not acknowledged
	/// @retval error_t::IO - internal IO error or bad bus state
	virtual error_t txrx(
		i2c_packet * packet
	) override;

	/// TWIM ISR handler. Must be a managed/OS-aware ISR.
	void twim_isr();

private:
	TWI_t volatile * m_twi;
	/// Semaphore to synchronize between txrx() and the ISR
	OS_EVENT * m_sem_sync;
	/// Semaphore to lock operation when another thread is transmitting
	OS_EVENT * m_sem_lock;

	// Data for the active transmission
	i2c_packet * m_packet;
	volatile uint8_t m_out_index;
	volatile uint8_t m_in_index;
	volatile enum class state {
		IDLE,

		TRANSMITTING,
		RECEIVING,

		/// "Idle" but still waiting for txrx() to unlock
		DONE,
		ERROR,
		ADDR_NAK
	} m_state;
};

// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

} // avl
#endif // !defined(AVL_AVRDX_TWI_HPP)
