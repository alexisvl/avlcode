// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef AVL_COMPORT_I_HPP
#define AVL_COMPORT_I_HPP

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include "avl_error.hpp"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

namespace avl {

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------

/// COM port interface. Supports nonblocking tx/rx, hard and soft flow control.
///
/// The implementation may have methods that need to be called from ISRs.
struct comport_i {

	/// Queue one byte for transmission.
	///
	/// If `timeout` is 0, this call must never block.
	///
	/// @param b - byte to transmit
	/// @param timeout - number of RTOS ticks to wait
	///
	/// @retval error_t::OK - byte queued
	/// @retval error_t:BUSY - timed out
	virtual error_t write(uint8_t b, uint16_t timeout) = 0;

	/// Queue a string for transmission. If possible, the implementation
	/// should enqueue the entire string at once, to allow for outputs from
	/// multiple threads to not interleave.
	///
	/// @param s - string to transmit
	/// @param len - length of string
	/// @param timeout - number of RTOS ticks to wait
	///
	/// @retval error_t::OK - string queued
	/// @retval error_t::BUSY - timed out
	/// @retval error_t::MSGSIZE - message too long to enqueue together
	virtual error_t write(uint8_t const * s, size_t len, uint16_t timeout)
		= 0;

	/// Queue a formatted string for transmission. If possible, the
	/// implementation should enqueue the entire string at once, to allow
	/// for outputs from multiple threads to not interleave.
	///
	/// Because fprintf outputs in a stream mode, this function should not
	/// fail if the message cannot fit in the queue. Suggested behavior is
	/// to transmit as much as possible, lock the queue and wait for space,
	/// and continue.
	///
	/// Implementations are suggested to use the protected member `m_file`,
	/// which is an stdio-compatible FILE object wrapping write() and
	/// read().
	///
	/// @param timeout - number of RTOS ticks to wait
	/// @param fmt - format string
	/// @param ap - argument list
	///
	/// @retval error_t::OK - string queued
	/// @retval error_t::BUSY - timed out
	virtual error_t vprintf(uint32_t timeout, char const * fmt, va_list ap)
		= 0;

	error_t printf(uint16_t timeout, char const * fmt, ...)
	{
		va_list ap;
		va_start(ap, fmt);
		return vprintf(timeout, fmt, ap);
	}

	error_t printf(char const * fmt, ...)
	{
		va_list ap;
		va_start(ap, fmt);
		return vprintf(UINT32_MAX, fmt, ap);
	}

	/// Receive one byte.
	///
	/// If `timeout` is 0, this call must never block.
	///
	/// @param timeout - number of RTOS ticks to wait
	///
	/// @return the byte received
	/// @return error_t::BUSY - timed out
	virtual result_t<uint8_t> read(uint16_t timeout) = 0;

	/// Enable or disable input xon/off. When enabled, implementation
	/// should pause transmission when XOFF is received, dropping that
	/// character, until XON is received (and also dropped). When disabled,
	/// no byte values should be dropped.
	///
	/// If XON and XOFF bytes are already in the input queue when this is
	/// switched on, they should still be returned.
	virtual void input_xonxoff(bool en) = 0;

	/// Enable or disable output xon/xoff. When enabled, implementation
	/// should send XOFF when its queue is nearly full, and XON again when
	/// the queue depletes (thresholds are defined by implementation).
	virtual void output_xonxoff(bool en) = 0;

	/// Enable or disable RTS/CTS flow control. When enabled,
	/// implementation should stop transmitting while RTS is deasserted,
	/// and should deassert CTS when not ready. The method of establishing
	/// control over the RTS and CTS lines is defined by implementation.
	///
	/// @retval error_t::OK: enabled or disabled successfully
	/// @retval error_t::NOSYS: no hardware flow control supported
	virtual error_t rtscts(bool en) = 0;

	comport_i() {
		m_file = (FILE) {
			.buf = NULL,
				.unget = 0,
				.flags = _FDEV_SETUP_WRITE,
				.size = 0,
				.len = 0,
				.put = _put,
				.get = _get,
				.udata = static_cast<void*>(this),
		};
	}

	protected:
	FILE m_file;

	/// Enqueue a byte for transmission, but don't immediately trigger the
	/// transmitter. This is used by the FILE wrapper to allow several
	/// bytes to be enqueued first, otherwise immediate DRE interrupt
	/// triggering could lock us into unwanted synchronous operation.
	///
	/// This should also be non-locking, assuming any locks are already
	/// held.
	///
	/// If the queue is full, this should call trigger_transmit() while
	/// waiting.
	virtual error_t write_not_immediate(uint8_t b) = 0;

	/// Trigger the transmitter after write_not_immediate. Redundant
	/// triggers should be harmless.
	virtual void trigger_transmit() = 0;

	static int _put(char c, FILE * f) {
		comport_i *com = static_cast<comport_i *>(f->udata);
		error_t e;
		do {
			e = com->write_not_immediate(static_cast<uint8_t>(c));
		} while (e == error_t::BUSY);
		return e != error_t::OK;
	}

	static int _get(FILE * f) {
		comport_i *com = static_cast<comport_i *>(f->udata);
		result_t<uint8_t> r = error_t::BUSY;
		do {
			r = com->read(UINT16_MAX);
		} while(r.err() == error_t::BUSY);
		return r.ok_or(-1);
	}
};

// --- PUBLIC CONSTANTS --------------------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

} // avl
#endif // !defined(AVL_COMPORT_I_HPP)
