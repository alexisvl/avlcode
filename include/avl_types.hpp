// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef AVL_TYPES_HPP
#define AVL_TYPES_HPP

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include "c++misc.hpp"

#ifdef __AVR__

/// Map into the AVR-DX's memory-mapped flash window. This allows data stored
/// in flash to be accessed without LPM.
#define FDAT __attribute__((section(".fdat")))

/// Create an FDAT string.
#define FSTR(s) ({static FDAT char const str[] = (s); str;})

#endif

namespace avl {

// --- PUBLIC MACROS -----------------------------------------------------------

/// Return a charstring_t given a string literal. On AVR, this is a PROGMEM
/// string
#ifdef __AVR__
#define CHARSTRING(s) FSTR(s)
#else
#define CHARSTRING(s) ((char const *)(s))
#endif

#define DECL_CREG(name, subtype, allbits) \
    struct name: public creg<subtype, allbits> { \
        constexpr name(subtype value, subtype mask): creg(value, mask) {} \
        constexpr name operator+(name const & other) const { \
            return creg::operator+(other); \
        } \
        constexpr name operator+() const { \
            return name(value, allbits); \
        } \
    }

// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------

/// (Deprecated, AVR should use FSTR)
typedef char const * charstring_t;

/// Configuration register value
template<typename T, T allbits>
struct creg {
    T value;
    T mask;

    constexpr creg(T value, T mask) : value(value), mask(mask) {}

    void apply_to(T volatile & reg) {
        if (mask == allbits) {
            reg = value;
        } else {
            reg = (reg & ~mask) | value;
        }
    }

    static constexpr T all_bits = allbits;

protected:
    template<typename Other>
    constexpr Other operator+(Other const & other) const {
        return Other(
            (value & ~other.mask) | (other.value & other.mask),
            mask | other.mask
        );
    }
};

// --- PUBLIC CONSTANTS --------------------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

} // avl

#endif // !defined(AVL_TYPES_HPP)
