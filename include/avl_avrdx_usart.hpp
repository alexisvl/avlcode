// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef AVL_AVRDX_USART_HPP
#define AVL_AVRDX_USART_HPP

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include <avr/io.h>
#include "avl_types.hpp"
#include "avl_interfaces.hpp"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

namespace avl {

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC CONSTANTS --------------------------------------------------------

static constexpr uintptr_t USART0_ADDR = (uintptr_t) &USART0;
static constexpr uintptr_t USART1_ADDR = (uintptr_t) &USART1;
static constexpr uintptr_t USART2_ADDR = (uintptr_t) &USART2;
static constexpr uintptr_t USART3_ADDR = (uintptr_t) &USART3;
static constexpr uintptr_t USART4_ADDR = (uintptr_t) &USART4;
#ifdef USART5
static constexpr uintptr_t USART5_ADDR = (uintptr_t) &USART5;
#endif

// --- PUBLIC DATATYPES --------------------------------------------------------

// CTRLA, CTRLB, CTRLC, CTRLD, BAUDL, BAUDH
DECL_CREG(usart_ctrla_t, uint8_t, 0b11111101);
DECL_CREG(usart_ctrlb_t, uint8_t, 0b11011111);
DECL_CREG(usart_ctrlc_t, uint8_t, 0xFF);
DECL_CREG(usart_ctrld_t, uint8_t, 0xC0);
DECL_CREG(usart_baud_t, uint16_t, 0xFFFFu);

struct usart_ctrla {
    static constexpr auto none = usart_ctrla_t(0, usart_ctrla_t::all_bits);
    struct rxcie {
        static constexpr auto no  = usart_ctrla_t(0,              USART_RXCIE_bm);
        static constexpr auto yes = usart_ctrla_t(USART_RXCIE_bm, USART_RXCIE_bm);
    };
    struct txcie {
        static constexpr auto no  = usart_ctrla_t(0,              USART_TXCIE_bm);
        static constexpr auto yes = usart_ctrla_t(USART_TXCIE_bm, USART_TXCIE_bm);
    };
    struct dreie {
        static constexpr auto no  = usart_ctrla_t(0,              USART_DREIE_bm);
        static constexpr auto yes = usart_ctrla_t(USART_DREIE_bm, USART_DREIE_bm);
    };
    struct rxsie {
        static constexpr auto no  = usart_ctrla_t(0,              USART_RXSIE_bm);
        static constexpr auto yes = usart_ctrla_t(USART_RXSIE_bm, USART_RXSIE_bm);
    };
    struct abeie {
        static constexpr auto no  = usart_ctrla_t(0,              USART_ABEIE_bm);
        static constexpr auto yes = usart_ctrla_t(USART_ABEIE_bm, USART_ABEIE_bm);
    };
    struct lbme {
        static constexpr auto no  = usart_ctrla_t(0,             USART_LBME_bm);
        static constexpr auto yes = usart_ctrla_t(USART_LBME_bm, USART_LBME_bm);
    };
    struct rs485 {
        static constexpr auto no  = usart_ctrla_t(0,              USART_RS485_bm);
        static constexpr auto yes = usart_ctrla_t(USART_RS485_bm, USART_RS485_bm);
    };
};

struct usart_ctrlb {
    static constexpr auto none = usart_ctrlb_t(0, usart_ctrlb_t::all_bits);
    struct rxen {
        static constexpr auto no  = usart_ctrlb_t(0, USART_RXEN_bm);
        static constexpr auto yes = usart_ctrlb_t(USART_RXEN_bm, USART_RXEN_bm);
    };
    struct txen {
        static constexpr auto no  = usart_ctrlb_t(0, USART_TXEN_bm);
        static constexpr auto yes = usart_ctrlb_t(USART_TXEN_bm, USART_TXEN_bm);
    };
    struct sfd {
        static constexpr auto no  = usart_ctrlb_t(0, USART_SFDEN_bm);
        static constexpr auto yes = usart_ctrlb_t(USART_SFDEN_bm, USART_SFDEN_bm);
    };
    struct odm {
        static constexpr auto no  = usart_ctrlb_t(0, USART_ODME_bm);
        static constexpr auto yes = usart_ctrlb_t(USART_ODME_bm, USART_ODME_bm);
    };
    struct rxmode {
        static constexpr auto normal  = usart_ctrlb_t(USART_RXMODE_NORMAL_gc, USART_RXMODE_gm);
        static constexpr auto clk2x   = usart_ctrlb_t(USART_RXMODE_CLK2X_gc, USART_RXMODE_gm);
        static constexpr auto genauto = usart_ctrlb_t(USART_RXMODE_GENAUTO_gc, USART_RXMODE_gm);
        static constexpr auto linauto = usart_ctrlb_t(USART_RXMODE_LINAUTO_gc, USART_RXMODE_gm);
    };
    struct mpcm {
        static constexpr auto no  = usart_ctrlb_t(0, USART_MPCM_bm);
        static constexpr auto yes = usart_ctrlb_t(USART_MPCM_bm, USART_MPCM_bm);
    };
};

struct usart_ctrlc {
    static constexpr auto none = usart_ctrlc_t(0, usart_ctrlc_t::all_bits);
    struct cmode {
        static constexpr auto async = usart_ctrlc_t(USART_CMODE_ASYNCHRONOUS_gc, USART_CMODE_gm);
        static constexpr auto sync  = usart_ctrlc_t(USART_CMODE_SYNCHRONOUS_gc, USART_CMODE_gm);
        static constexpr auto ircom = usart_ctrlc_t(USART_CMODE_IRCOM_gc, USART_CMODE_gm);
        static constexpr auto mspi  = usart_ctrlc_t(USART_CMODE_MSPI_gc, USART_CMODE_gm);
    };
    struct parity {
        static constexpr auto none = usart_ctrlc_t(USART_PMODE_DISABLED_gc, USART_PMODE_gm);
        static constexpr auto even = usart_ctrlc_t(USART_PMODE_EVEN_gc, USART_PMODE_gm);
        static constexpr auto odd  = usart_ctrlc_t(USART_PMODE_ODD_gc, USART_PMODE_gm);
    };
    struct stopbits {
        static constexpr auto one = usart_ctrlc_t(0, USART_SBMODE_bm);
        static constexpr auto two = usart_ctrlc_t(USART_SBMODE_bm, USART_SBMODE_bm);
    };
    struct chsize {
        static constexpr auto five  = usart_ctrlc_t(USART_CHSIZE_5BIT_gc, USART_CHSIZE_gm);
        static constexpr auto six   = usart_ctrlc_t(USART_CHSIZE_6BIT_gc, USART_CHSIZE_gm);
        static constexpr auto seven = usart_ctrlc_t(USART_CHSIZE_7BIT_gc, USART_CHSIZE_gm);
        static constexpr auto eight = usart_ctrlc_t(USART_CHSIZE_8BIT_gc, USART_CHSIZE_gm);
        static constexpr auto nine  = usart_ctrlc_t(USART_CHSIZE_9BITH_gc, USART_CHSIZE_gm);
    };
};

struct usart_baud {
    template<uint32_t Baud, uint32_t Fcpu = F_CPU>
    static constexpr usart_baud_t baud() {
        constexpr uint32_t baudreg = (uint32_t)(4uL * Fcpu) / (uint32_t) Baud;
        static_assert(baudreg <= 0xFFFFuL, "baud rate too low");
        static_assert(baudreg >= 64, "baud rate too high (maybe try usart_baud_2x?)");

        return usart_baud_t(baudreg, 0xFFFFu);
    }

    template<uint32_t Baud, uint32_t Fcpu = F_CPU>
    static constexpr usart_baud_t baud_clk2x() {
        constexpr uint32_t baudreg = (uint32_t)(8uL * Fcpu) / (uint32_t) Baud;
        static_assert(baudreg <= 0xFFFFuL, "baud rate too low");
        static_assert(baudreg >= 64, "baud rate too high");

        return usart_baud_t(baudreg, 0xFFFFu);
    }

    /// Calculate the baud value at runtime from a baudrate.
    ///
    /// Returns error_t::INVAL if invalid.
    static result_t<usart_baud_t> calc_baud(uint32_t baudrate, uint32_t f_cpu)
    {
        uint32_t baudreg = (uint32_t)(4uL * f_cpu) / (uint32_t) baudrate;

        if (baudreg > 0xFFFFuL || baudreg < 64) {
            return error_t::INVAL;
        } else {
            return usart_baud_t(baudreg, 0xFFFFu);
        }
    }

    /// Calculate the baud value at runtime from a baudrate.
    ///
    /// Returns error_t::INVAL if invalid.
    static result_t<usart_baud_t> calc_baud_clk2x(uint32_t baudrate, uint32_t f_cpu)
    {
        uint32_t baudreg = (uint32_t)(8uL * f_cpu) / (uint32_t) baudrate;

        if (baudreg > 0xFFFFuL || baudreg < 64) {
            return error_t::INVAL;
        } else {
            return usart_baud_t(baudreg, 0xFFFFu);
        }
    }
};

/// USART.
///
/// @param usartaddr - address of the USART, USART0_ADDR through USART5_ADDR
template<uintptr_t usartaddr>
struct usart: public byte_ostream {
    union rxdata {
        struct {
            unsigned data : 9;
            bool perr : 1;
            bool ferr : 1;
            unsigned _res : 3;
            bool bufovf : 1;
            bool rxcif : 1;
        };
        uint8_t raw[2];
    };

    usart& operator<<(usart_ctrla_t cfg) {
        cfg.apply_to(regs().CTRLA);
        return *this;
    }
    usart& operator<<(usart_ctrlb_t cfg) {
        cfg.apply_to(regs().CTRLB);
        return *this;
    }
    usart& operator<<(usart_ctrlc_t cfg) {
        cfg.apply_to(regs().CTRLC);
        return *this;
    }
    usart& operator<<(usart_ctrld_t cfg) {
        cfg.apply_to(regs().CTRLD);
        return *this;
    }
    usart& operator<<(usart_baud_t cfg) {
        cfg.apply_to(regs().BAUD);
        return *this;
    }

    void write(uint16_t ch) {
        regs().TXDATAH = ch >> 8;
        regs().TXDATAL = ch & 0xFF;
    }

    rxdata read() {
        uint8_t const rxh = regs().RXDATAH;
        uint8_t const rxl = regs().RXDATAL;

        rxdata rx = {.raw = {rxl, rxh}};
        return rx;
    }

    /// Check if the Receive Complete flag is set.
    bool rxc() {
        return regs().STATUS & USART_RXCIF_bm;
    }

    /// Check if the Transmit Complete flag is set
    ///
    /// @param clear - whether to clear it if set.
    bool txc(bool clear) {
        uint8_t const stat = regs().STATUS & USART_TXCIF_bm;
        if (clear) {
            regs().STATUS = stat;
        }
        return stat;
    }

    /// Check if the Data Register Empty flag is set.
    bool dre() {
        return regs().STATUS & USART_DREIF_bm;
    }

    constexpr USART_t & regs() {
        return *(USART_t *) usartaddr;
    }

    // -- implementation: byte_ostream -----------------------------------------
    virtual void send(uint8_t b) override {
        while (!dre());
        write(b);
    }
};

// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

} // avl

#endif // !defined(AVL_AVRDX_USART_HPP)
