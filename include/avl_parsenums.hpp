// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef AVL_PARSENUMS_HPP
#define AVL_PARSENUMS_HPP

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include "avl_error.hpp"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

namespace avl {

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------
// --- PUBLIC CONSTANTS --------------------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

/// Parse a hex nibble.
///
/// @return value
/// @retval error_t::INVAL - not a valid hex nibble
result_t<uint8_t> parse_hex_nibble(char c);

char to_hex_nibble(uint8_t n);

/// Parse an integer.
///
/// Negatives are accepted; cast the return value to int32_t.
///
/// @param s - string to parse
/// @param base - base to parse in. If 0, looks for a prefix of 0x/0o/0b
///   (case insensitive). Must be 16 or lower.
/// @retval error_t::INVAL - not a valid integer
result_t<uint32_t> parse_int(char const * s, uint8_t base);

} // avl
#endif // !defined(AVL_PARSENUMS_HPP)
