// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

// TODO: document this elsewhere
//
// isrsafe:
//      yes         This function is inherently safe in a multithreaded/isr
//                  environment, as long as all data passed to it also is.
//      atomicblock This function achieves interrupt safety using an
//                  ATOMIC_BLOCK(). It briefly disables interrupts, causing some
//                  interrupt jitter. Interrupt state will be restored to its
//                  previous state.
//      same-device This function is interrupt-safe as long as other operations
//                  on the same devices are only performed in this context. It
//                  will not interfere with non-atomic operations on other
//                  devices.

#ifndef AVL_AVRDX_PORT_HPP
#define AVL_AVRDX_PORT_HPP

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include <avr/io.h>
#include <util/atomic.h>
#include "avl_types.hpp"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

namespace avl {

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC CONSTANTS --------------------------------------------------------

static constexpr uintptr_t PORTA_ADDR = (uintptr_t) &PORTA;
static constexpr uintptr_t PORTB_ADDR = (uintptr_t) &PORTB;
static constexpr uintptr_t PORTC_ADDR = (uintptr_t) &PORTC;
static constexpr uintptr_t PORTD_ADDR = (uintptr_t) &PORTD;
static constexpr uintptr_t PORTE_ADDR = (uintptr_t) &PORTE;
static constexpr uintptr_t PORTF_ADDR = (uintptr_t) &PORTF;
#ifdef PORTG
static constexpr uintptr_t PORTG_ADDR = (uintptr_t) &PORTG;
#endif

// --- PUBLIC DATATYPES --------------------------------------------------------

DECL_CREG(port_pinctrl_t, uint8_t, 0xCF);
DECL_CREG(port_dir_t, uint8_t, 0x03);

struct port_pinctrl {
    static constexpr auto reset = port_pinctrl_t(0, 0xFF);

    struct invert {
        static constexpr auto no  = port_pinctrl_t(0,             PORT_INVEN_bm);
        static constexpr auto yes = port_pinctrl_t(PORT_INVEN_bm, PORT_INVEN_bm);
    };
    struct level {
        static constexpr auto schmitt = port_pinctrl_t(0,             PORT_INLVL_bm);
        static constexpr auto ttl     = port_pinctrl_t(PORT_INLVL_bm, PORT_INLVL_bm);
    };
    struct pullup {
        static constexpr auto no  = port_pinctrl_t(0,                PORT_PULLUPEN_bm);
        static constexpr auto yes = port_pinctrl_t(PORT_PULLUPEN_bm, PORT_PULLUPEN_bm);
    };
    struct isc {
        static constexpr auto intdisable    = port_pinctrl_t(PORT_ISC_INTDISABLE_gc,    PORT_ISC_gm);
        static constexpr auto bothedges     = port_pinctrl_t(PORT_ISC_BOTHEDGES_gc,     PORT_ISC_gm);
        static constexpr auto rising        = port_pinctrl_t(PORT_ISC_RISING_gc,        PORT_ISC_gm);
        static constexpr auto falling       = port_pinctrl_t(PORT_ISC_FALLING_gc,       PORT_ISC_gm);
        static constexpr auto input_disable = port_pinctrl_t(PORT_ISC_INPUT_DISABLE_gc, PORT_ISC_gm);
        static constexpr auto low_level     = port_pinctrl_t(PORT_ISC_LEVEL_gc,         PORT_ISC_gm);
    };
};

struct port_dir {
private:
    static constexpr uint8_t OUT1   = 0b001;
    static constexpr uint8_t PRESET = 0b010;
    static constexpr uint8_t DIR    = 0b100;
    static constexpr uint8_t ALL    = 0b111;

public:
    static constexpr auto reset = port_dir_t(0, 0xFF);

    static constexpr auto input    = port_dir_t(0, ALL);
    static constexpr auto output   = port_dir_t(DIR, ALL);
    static constexpr auto output_0 = port_dir_t(DIR | PRESET, ALL);
    static constexpr auto output_1 = port_dir_t(DIR | PRESET | OUT1, ALL);

    template<uintptr_t, unsigned> friend class pin;
    template<uintptr_t, uint8_t>  friend class pin_group;
};

/// Single IO pin.
///
/// @param portaddr - a port address, PORTA_ADDR through PORTG_ADDR
/// @param npin - a pin number, 0 through 7
template<uintptr_t Portaddr, unsigned Npin>
class pin {
public:
    static_assert(Npin <= 7, "npin must be no more than 7");

    /// Set pinctrl flags.
    ///
    /// isrsafe: same-device
    pin& operator<<(port_pinctrl_t cfg) {
        cfg.apply_to(pinctrl());
        return *this;
    }

    /// Set direction and default output value.
    ///
    /// isrsafe: same-device
    pin& operator<<(port_dir_t cfg) {
        const bool output = cfg.value & port_dir::DIR;
        const bool preset_output = cfg.value & port_dir::PRESET;
        const bool preset_value = cfg.value & port_dir::OUT1;

        if (!output) {
            vport().DIR &= ~(1u << Npin);
        } else if (output && !preset_output) {
            vport().DIR |= (1u << Npin);
        } else if (output && preset_output) {
            if (preset_value) {
                vport().OUT |= (1u << Npin);
            } else {
                vport().OUT &= ~(1u << Npin);
            }
            vport().DIR |= (1u << Npin);
        }
        return *this;
    }

    /// Write this pin.
    ///
    /// isrsafe: yes
    pin& operator=(bool v) {
        if (v) {
            vport().OUT |= (1u << Npin);
        } else {
            vport().OUT &= ~(1u << Npin);
        }
        return *this;
    }

    /// Toggle this pin.
    ///
    /// isrsafe: yes
    void toggle() {
        port().OUTTGL = 1u << Npin;
    }

    /// Read this pin.
    ///
    /// isrsafe: yes
    bool operator*() {
        return vport().IN & (1u << Npin);
    }

    /// Get and clear the interrupt flag for this pin.
    ///
    /// isrsafe: yes
    /// @return whether an interrupt flag is set
    bool get_and_clear_intflag() {
        uint8_t const flag = vport().INTFLAGS & (1u << Npin);
        vport().INTFLAGS = flag;
        return flag;
    }

    /// Return a reference to the port register struct
    constexpr PORT_t & port() {
        return *(PORT_t *) Portaddr;
    }

    /// Return a reference to the pin(n)ctrl register
    constexpr register8_t & pinctrl() {
        return (&port().PIN0CTRL)[Npin];
    }

    /// Return a reference to the vport register struct
    constexpr VPORT_t & vport() {
        return (&VPORTA)[(Portaddr - (uintptr_t)&PORTA)/sizeof(PORT_t)];
    }

    /// Return the pin number
    constexpr uint8_t npin() {
        return Npin;
    }
};

/// Wrapper for a pin that makes it open-drain. You still must configure it
/// sensibly (e.g. default to port_dir::input or port_dir::output_0); it will
/// toggle DIR instead of OUT.
template<uintptr_t portaddr, unsigned npin>
struct pin_opendrain: public pin<portaddr, npin>
{
    /// Write this pin.
    ///
    /// isrsafe: yes
    pin_opendrain& operator=(bool v) {
        if (v) {
            pin<portaddr, npin>::vport().DIR &= ~(1u << npin);
        } else {
            pin<portaddr, npin>::vport().DIR |= (1u << npin);
        }
        return *this;
    }

    /// Toggle this pin.
    ///
    /// isrsafe: yes
    void toggle() {
        pin<portaddr, npin>::port().DIRTGL = 1u << npin;
    }
};

/// Group of IO pins on one port.
///
/// @param portaddr - a port address, PORTA_ADDR through PORTG_ADDR
/// @param mask - bit mask of pins
template<uintptr_t portaddr, uint8_t mask>
class pin_group {
public:
    /// Set pinctrl flags.
    ///
    /// isrsafe: atomicblock
    pin_group& operator<<(port_pinctrl_t cfg) {
        ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
            port().PINCONFIG = cfg.value;
            port().PINCTRLUPD = mask;
        }
        return *this;
    }

    /// Set direction and default output value.
    ///
    /// isrsafe: same-device
    pin_group& operator<<(port_dir_t cfg) {
        const bool output = cfg.value & port_dir::DIR;
        const bool preset_output = cfg.value & port_dir::PRESET;
        const bool preset_value = cfg.value & port_dir::OUT1;

        if (!output) {
            vport().DIR &= ~mask;
        } else if (output && !preset_output) {
            vport().DIR |= mask;
        } else if (output && preset_output) {
            if (preset_value) {
                vport().OUT |= mask;
            } else {
                vport().OUT &= ~mask;
            }
            vport().DIR |= mask;
        }
        return *this;
    }

    /// Write these pins.
    ///
    /// isrsafe: (mask == 0xFF) ? same-device : atomicblock
    pin_group& operator=(uint8_t v) {
        if (mask == 0xFF) {
            vport().OUT = v;
        } else {
            ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
                vport().OUT = (vport().OUT & ~mask) | (v & mask);
            }
        }
        return *this;
    }

    /// Set the specified pins, atomically.
    ///
    /// isrsafe: yes
    pin_group& operator+=(uint8_t v) {
        if (__builtin_constant_p(v) && __builtin_popcount(v) == 1) {
            // this is only the better choice if we're only setting one bit
            // (it'll use sbi)
            vport().OUT |= (v & mask);
        } else {
            port().OUTSET = (v & mask);
        }
        return *this;
    }

    /// Clear the specified pins, atomically.
    ///
    /// isrsafe: yes
    pin_group& operator-=(uint8_t v) {
        if (__builtin_constant_p(v) && __builtin_popcount(v) == 1) {
            // this is only the better choice if we're only setting one bit
            // (it'll use cbi)
            vport().OUT &= ~(v & mask);
        } else {
            port().OUTCLR = (v & mask);
        }
        return *this;
    }

    /// Toggle the specified pins, atomically.
    ///
    /// isrsafe: yes
    pin_group& operator^=(uint8_t v) {
        port().OUTTGL = (v & mask);
        return *this;
    }

    /// Read this pin group.
    ///
    /// isrsafe: yes
    uint8_t operator*() {
        return vport().IN & mask;
    }

    /// Get and clear the interrupt flag for this pin group.
    ///
    /// isrsafe: yes
    /// @return field of interrupt flags
    uint8_t get_and_clear_intflag() {
        uint8_t const flag = vport().INTFLAGS & mask;
        vport().INTFLAGS = flag;
        return flag;
    }

    /// Return a reference to the port register struct
    constexpr PORT_t & port() {
        return *(PORT_t *) portaddr;
    }

    /// Return a reference to the vport register struct
    constexpr VPORT_t & vport() {
        return (&VPORTA)[(portaddr - (uintptr_t)&PORTA)/sizeof(PORT_t)];
    }
};

/// Wrapper for a pin group that makes it open-drain. You still must configure
/// it sensibly (e.g. default to port_dir::input or port_dir::output_0); it will
/// toggle DIR instead of OUT.
template<uintptr_t portaddr, uint8_t mask>
struct pin_group_opendrain: public pin_group<portaddr, mask>
{
    /// Write this pin.
    ///
    /// isrsafe: (mask == 0xFF) ? same-device ? atomicblock
    pin_group_opendrain& operator=(uint8_t v) {
        if (mask == 0xFF) {
            pin_group<portaddr, mask>::vport().DIR = ~v;
        } else {
            ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
                pin_group<portaddr, mask>::vport().DIR =
                    (pin_group<portaddr, mask>::vport().DIR & ~mask) | (~v & mask);
            }
        }
        return *this;
    }

    /// Set the specified pins, atomically.
    ///
    /// isrsafe: yes
    pin_group_opendrain& operator+=(uint8_t v) {
        pin_group<portaddr, mask>::port().DIRCLR = (v & mask);
        return *this;
    }

    /// Clear the specified pins, atomically.
    ///
    /// isrsafe: yes
    pin_group_opendrain& operator-=(uint8_t v) {
        pin_group<portaddr, mask>::port().DIRSET = (v & mask);
        return *this;
    }

    /// Toggle the specified pins, atomically.
    ///
    /// isrsafe: yes
    pin_group_opendrain& operator^=(uint8_t v) {
        pin_group<portaddr, mask>::port().DIRTGL = (v & mask);
        return *this;
    }
};

// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

} // avl

#endif // !defined(AVL_AVRDX_PORT_HPP)
