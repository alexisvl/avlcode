// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef AVL_I2C_I_HPP
#define AVL_I2C_I_HPP

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include "avl_error.hpp"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

namespace avl {

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------

/// I2C packet definition
struct i2c_packet {
	/// 7-bit address, left-aligned (bit 0 will be ignored)
	uint8_t addr;

	/// Number of bytes to transmit. Will be updated with the number of
	/// bytes actually transmitted
	uint8_t n_out;

	/// Number of bytes to receive. Will be updated with the number of
	/// bytes actually received
	uint8_t n_in;

	/// Timeout in ticks. This timeout need not govern the entire bus
	/// operation, only intermediate synchronizations (so the entire
	/// operation may take longer).
	uint8_t timeout;

	/// Data buffer for both receive and transmit
	uint8_t * buffer;
};

/// Simple I2C peripheral.
///
/// Implementation is encouraged to use RTOS blocking via ISR when possible.
struct i2c_i {
	/// Packet transaction. buffer[0:n_out] is sent, then buffer[0:n_in]
	/// is received into.
	///
	/// If both n_out and n_in are zero, buffer may be null (this allows
	/// probing an address for ack/nack).
	///
	/// Counts will be updated with the number of bytes actually sent. Note
	/// that partial receive due to receipt of a NACK do not constitute
	/// errors.
	///
	/// If n_in is zero, implementation must never write to buffer[].
	///
	/// @param packet - packet to transmit and receive
	///
	/// @retval error_t::OK - success
	/// @retval error_t::NODEV - NACK on address
	///
	/// Other return values are permitted.
	virtual error_t txrx(
		i2c_packet * packet
	) = 0;
};

// --- PUBLIC CONSTANTS --------------------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

}

#endif // !defined(AVL_I2C_I_HPP)
