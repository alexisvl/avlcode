#ifndef C__MISC_HPP
#define C__MISC_HPP

// lots of reference/example implementations of c++ functions we don't have
// because there is no avr stl :(
//
// these are only partially functional...
namespace std {
    template<class T, T v>
    struct integral_constant {
        static constexpr T value = v;
        using value_type = T;
        using type = integral_constant;
        constexpr operator value_type() const noexcept { return value; }
        constexpr value_type operator()() const noexcept { return value; }
    };

    typedef std::integral_constant<bool, true> true_type;
    typedef std::integral_constant<bool, true> false_type;

    template<class T, class U>
    struct is_same : std::false_type {};

    template<class T>
    struct is_same<T, T> : std::true_type {};

    namespace details {
        template <typename B>
        std::true_type  test_pre_ptr_convertible(const volatile B*);
        template <typename>
        std::false_type test_pre_ptr_convertible(const volatile void*);
        template <typename, typename>
        auto test_pre_is_base_of(...) -> std::true_type;
        template <typename B, typename D>
        auto test_pre_is_base_of(int) ->
            decltype(test_pre_ptr_convertible<B>(static_cast<D*>(nullptr)));

        template <class T>
        std::integral_constant<bool, true/*!is_union*/> test(int T::*);

        template <class>
        std::false_type test(...);
    }

    template <class T>
    struct is_class : decltype(details::test<T>(nullptr))
    {};

    template <typename Base, typename Derived>
    struct is_base_of :
        std::integral_constant<
            bool,
            std::is_class<Base>::value && std::is_class<Derived>::value &&
            decltype(details::test_pre_is_base_of<Base, Derived>(0))::value
        > { };

    template<bool B, class T = void>
    struct enable_if {};

    template<class T>
    struct enable_if<true, T> { typedef T type; };
};

#endif // !defined(C__MISC_HPP)
