// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef AVL_COMPORT_AVRDX_HPP
#define AVL_COMPORT_AVRDX_HPP

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include "avl_comport_i.hpp"
#include "avl_avrdx_usart.hpp"
#include "avl_avrdx_port.hpp"
#include "avl_queue.hpp"
#include <ucos_ii.h>

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <stdio.h>
#include <util/atomic.h>

#pragma GCC push_options
#pragma GCC optimize ("-Os")

namespace avl {

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------

/// USART error flags
enum class usart_error: uint8_t {
    NONE        = 0,
    PARITY      = 0x01,
    FRAME       = 0x02,
    HW_OVERFLOW = 0x04,
    SW_OVERFLOW = 0x08,
};

/// Implementation of avl_comport_i for AVR-DX USART and uC/OS. ISR-driven.
template<
    typename Usart, ///< USART instance to use
    typename Rts = int*, ///< RTS pin - assumed non-inverting, use INVEN
    typename Cts = int,  ///< CTS pin - assumed non-inverting, use INVEN
    uint8_t tx_queue_len = 80,   ///< Size of tx queue
    uint8_t rx_queue_len = 80    ///< Size of rx queue
>
struct comport_avrdx: public comport_i
{
protected:
    virtual error_t write_not_immediate(uint8_t b) override
    {
        error_t e = error_t::BUSY;

        do {
            result_t<uint8_t*> place = m_tx_queue.emplace_ptr();
            if (place.is_ok()) {
                *place.ok() = b;
                m_tx_queue.push_emplaced();
                e = error_t::OK;
            }

            if (e == error_t::BUSY) {
                trigger_transmit();
                OSTimeDly(1);
            }
        } while (e == error_t::BUSY);

        return e;
    }

    virtual void trigger_transmit() override {
        usart() << usart_ctrla::dreie::yes;
    }
public:
    virtual error_t write(uint8_t b, uint16_t timeout) override
    {
        uint8_t oserr = 0;
        OSSemPend(m_sem_tx, timeout, &oserr);
        if (oserr == OS_ERR_TIMEOUT) {
            return error_t::BUSY;
        } else if (oserr != OS_ERR_NONE) {
            return error_t::PERM;
        } else {
            error_t e = write_not_immediate(b);
            trigger_transmit();
            OSSemPost(m_sem_tx);
            return e;
        }
    }

    virtual error_t write(uint8_t const * s, size_t len, uint16_t timeout) override
    {
        error_t e = error_t::BUSY;
        uint8_t oserr = 0;

        OSSemPend(m_sem_tx, timeout, &oserr);

        if (oserr == OS_ERR_TIMEOUT) {
            return error_t::BUSY;
        } else if (oserr != OS_ERR_NONE) {
            return error_t::PERM;
        } else {
            for (size_t i = 0; i < len;) {
                result_t<uint8_t*> place = m_tx_queue.emplace_ptr();
                if (place.is_ok()) {
                    *place.ok() = s[i++];
                    m_tx_queue.push_emplaced();
                } else {
                    trigger_transmit();
                    OSTimeDly(1);
                }
            }

            OSSemPost(m_sem_tx);
            trigger_transmit();
            e = error_t::OK;
            return e;
        }
    }

    virtual error_t vprintf(uint32_t timeout, char const * fmt, va_list ap) override
    {
        error_t e = error_t::BUSY;
        uint8_t oserr = 0;

        OSSemPend(m_sem_tx, timeout, &oserr);

        if (oserr == OS_ERR_TIMEOUT) {
            return error_t::BUSY;
        } else if (oserr != OS_ERR_NONE) {
            return error_t::PERM;
        } else {
            vfprintf(&m_file, fmt, ap);

            OSSemPost(m_sem_tx);
            trigger_transmit();
            e = error_t::OK;
            return e;
        }
    }

    /// Wait for and read a character.
    virtual result_t<uint8_t> read(uint16_t timeout) override
    {
        uint8_t oserr = 0;

        OSSemPend(m_sem_rx, timeout, &oserr);

        if (oserr == OS_ERR_TIMEOUT) {
            return error_t::BUSY;
        } else if (oserr != OS_ERR_NONE) {
            return error_t::PERM;
        } else {
            result_t<uint8_t> r = error_t::BUSY;

            ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
                r = m_rx_queue.pop();
            }
            check_and_set_cts();

            return r;
        }
    }

    virtual void input_xonxoff(bool en) override
    {
        m_input_xonxoff = en;
    }

    virtual void output_xonxoff(bool en) override
    {
        m_output_xonxoff = en;
    }

    virtual error_t rtscts(bool en) override
    {
        m_rtscts = en;
        return error_t::OK;
    }

    /// Return and clear error flags
    usart_error error()
    {
        usart_error e;

        ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
            e = m_error;
            m_error = 0;
        }

        return e;
    }

    /// RTS toggle interrupt handler. If using RTS/CTS, this must be called when
    /// RTS changes. It doesn't have to be called from an actual ISR, but it
    /// should be called expediently.
    void rts_toggled(bool rts_value)
    {
        ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
            m_rts = rts_value;
            if (rts_value) {
                usart() << usart_ctrla::dreie::yes;
            }
        }
    }

    /// DRE interrupt handler. This should be called in DRE_vect, which needs
    /// to be uC/OS-aware.
    void dre()
    {
        if (m_send_ctrl) {
            usart().write(m_send_ctrl);
            m_send_ctrl = 0;
            OSIntNoSched = 1;
            return;
        }

        if (!m_rts || !m_tx_is_xon) {
            usart() << usart_ctrla::dreie::no;
            OSIntNoSched = 1;
            return;
        }

        result_t<uint8_t> data = m_tx_queue.pop();

        if (!data.is_ok()) {
            usart() << usart_ctrla::dreie::no;
            OSIntNoSched = 1;
            return;
        }

        usart().write(data.ok());
    }

    /// RXC interrupt handler. This should be called in RXC_vect, which needs
    /// to be uC/OS-aware.
    void rxc()
    {
        auto rxdata = usart().read();
        bool error = false;

        if (rxdata.perr) {
            m_error |= (uint8_t) usart_error::PARITY;
            error = true;
        }

        if (rxdata.ferr) {
            m_error |= (uint8_t) usart_error::FRAME;
            error = true;
        }

        if (rxdata.bufovf) {
            m_error |= (uint8_t) usart_error::HW_OVERFLOW;
            error = true;
        }

        if (!error) {
            if (m_input_xonxoff && rxdata.data == XOFF) {
                m_tx_is_xon = false;
                OSIntNoSched = 1;
            } else if (m_input_xonxoff && rxdata.data == XON) {
                m_tx_is_xon = true;
                OSIntNoSched = 1;
            } else {
                result_t<uint8_t *> place = m_rx_queue.emplace_ptr();

                if (place.is_ok()) {
                    *place.ok() = static_cast<uint8_t>(rxdata.data);
                    m_rx_queue.push_emplaced();
                    OSSemPost(m_sem_rx);
                } else {
                    m_error |= (uint8_t) usart_error::SW_OVERFLOW;
                    OSIntNoSched = 1;
                }

                uint8_t weight = m_rx_queue.weight();

                if (weight >= RX_QUEUE_CLEAR_CTS && m_cts) {
                    m_cts = false;
                    cts(false);
                    if (m_output_xonxoff) {
                        m_send_ctrl = XOFF;
                        usart() << usart_ctrla::dreie::yes;
                    }
                }
            }
        } else {
            OSIntNoSched = 1;
        }
    }

    /// Return whether transmit is currently blocked due to RTS or XOFF
    bool blocked() const {
        return !m_rts || !m_tx_is_xon;
    }

    /// Return our usart instance (note this is a ZST)
    Usart usart() const {
        Usart u;
        return u;
    }

    comport_avrdx()
        : m_input_xonxoff(false)
        , m_output_xonxoff(false)
        , m_rtscts(false)
        , m_send_ctrl(0)
        , m_error((uint8_t) usart_error::NONE)
        , m_sem_tx(nullptr)
        , m_sem_rx(nullptr)
        , m_rts(true)
        , m_cts(true)
        , m_tx_is_xon(true)
    {
    }

    /// Initialize. Must be called after OS initialization.
    error_t init() {
        m_sem_tx = OSSemCreate(1);
        if (!m_sem_tx) {
            return error_t::NOSPC;
        }

        m_sem_rx = OSSemCreate(1);
        if (!m_sem_rx) {
            return error_t::NOSPC;
        }

        return error_t::OK;
    }

    static constexpr uint8_t XON = 17;
    static constexpr uint8_t XOFF = 19;
    static constexpr uint8_t RX_QUEUE_CLEAR_CTS = (3 * rx_queue_len + 1) / 4;
    static constexpr uint8_t RX_QUEUE_SET_CTS = (1 * rx_queue_len) / 8;
private:
    bool rts() {
        Rts r; // TODO: handle properly if this is just int*
        return m_rtscts ? *r : true;
    }

    void cts(bool en) {
        Cts c;
        (void) c;
        if (m_rtscts) {
            c = en;
        }
    }

    void check_and_set_cts() {
        uint8_t weight = m_rx_queue.weight();

        if (weight <= RX_QUEUE_SET_CTS && !m_cts) {
            m_cts = true;
            cts(true);
            if (m_output_xonxoff) {
                ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
                    m_send_ctrl = XON;
                    usart() << usart_ctrla::dreie::yes;
                }
            }
        }
    }

    queue<uint8_t, tx_queue_len> m_tx_queue;
    queue<uint8_t, rx_queue_len> m_rx_queue;

    bool m_input_xonxoff;
    bool m_output_xonxoff;
    bool m_rtscts;
    uint8_t m_send_ctrl; ///< Control byte (XON or XOFF) to send, or 0
    uint8_t m_error;
    OS_EVENT * m_sem_tx;
    OS_EVENT * m_sem_rx;
    bool m_rts;
    bool m_cts;
    bool m_tx_is_xon;
};

// --- PUBLIC CONSTANTS --------------------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

#pragma GCC pop_options
} // avl
#endif // !defined(AVL_COMPORT_AVRDX_HPP)
