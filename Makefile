#######################################################################
# CONFIG: Main configuration - what to build and what to make it into
SOURCES := \
	test_avr.cpp \
	amib/amib_ll.cpp \
	amib/fletcher16.c \
	amib/amib.c
OUTPUT  := test_avr.elf
OUTPUTS := ${OUTPUT} test_avr.disas
INCLUDES := \
	include \
	amib \
	.

#######################################################################
# CONFIG: Build configuration
MCU        ?= avr128db64
F_CPU      ?= 24000000
FLASHSZ    ?= 131072
RAMSZ      ?= 16384
EESZ       ?= 512
OPT_FLAGS  ?= -O2 -flto
WARN_FLAGS ?= -Wall -Wextra -Werror
MACH_FLAGS ?= -mrelax -maccumulate-args
CSTD       ?= gnu11
CXXSTD     ?= gnu++17
FLAGS = \
	-B${MICROCHOP_AVR} \
	--sysroot=${MICROCHOP_AVR} \
	-mmcu=${MCU} \
	-ggdb \
	${OPT_FLAGS} \
	${WARN_FLAGS} \
	${MACH_FLAGS} \
	${INCLUDES:%=-I%} \
	-DF_CPU=${F_CPU}uL \

CFLAGS = ${FLAGS} -std=${CSTD}
CXXFLAGS = ${FLAGS} -std=${CXXSTD}

#######################################################################
# CONFIG: Toolchain paths
TARGET  ?= avr-
CC      := ${TARGET}gcc
CXX     := ${TARGET}g++
OBJDUMP := ${TARGET}objdump
SIZE    := ${TARGET}size
MH      := python3 ./makehelp.py
MICROCHOP_AVR ?= \
    $(lastword $(sort $(wildcard /opt/microchip/xc8/v*.*/dfp/xc8/avr)))

#######################################################################
# Derived lists
obj1    := ${SOURCES:.cpp=.o}
obj2    := ${obj1:.c=.o}
OBJECTS := ${obj2}

dep1    := ${SOURCES:.cpp=.d}
dep2    := ${dep1:.c=.d}
DEPS    := ${dep2}

#######################################################################
# Support code

# If make runs ${MH} capturing output, it'll blow up when it tries to get the
# terminal width. This passes it in instead.
MH_FIXCOLS = ${MH} -c$(shell tput cols)
# Function to run ${MH} right now - outside a target - and print the output.
RUNMH = $(info $(shell ${MH_FIXCOLS} $(1)))
# Suffix gcc calls with this to format them nicely for ${MH}
GCC_FORMAT := -fdiagnostics-color=always -fmessage-length=$(shell tput cols)

ifeq (${MAKECMDGOALS},clean)
NODEP := 1
else
NODEP := 0
endif

#######################################################################
# HELLO... Compiling ${OUTPUT}
# ........
# blah blah blah
$(call RUNMH,hbar)
ifeq (${MAKECMDGOALS},clean)
$(call RUNMH,info :magenta:HELLO 🗑️ Cleaning up)
else
$(call RUNMH,info :magenta:HELLO 🛠️ Compiling ${OUTPUT})
$(call RUNMH,info :magenta: " ")
$(call RUNMH,info :magenta:target ${MCU} @ Fcpu = ${F_CPU})
$(call RUNMH,info :magenta:stds ${CSTD} ${CXXSTD})
$(call RUNMH,info :magenta:opt ${OPT_FLAGS})
$(call RUNMH,info :magenta:warn ${WARN_FLAGS})
$(call RUNMH,info :magenta:machine ${MACH_FLAGS})
$(call RUNMH,info :magenta:include ${INCLUDES})
endif
$(call RUNMH,hbar)

#######################################################################
# Targets
.PHONY: all clean

%.d: %.cpp
	@${MH} run :cyan:DEP $< -- ${CXX} -MM ${CXXFLAGS} $< -o $@.$$$$ ${GCC_FORMAT} && \
	sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@ && \
	rm -f $@.$$$$

%.d: %.c
	@${MH} run :cyan:DEP $< -- ${CC} -MM ${CFLAGS} $< -o $@.$$$$ ${GCC_FORMAT} && \
	sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@; \
	rm -f $@.$$$$

all: ${OUTPUTS}
	@${MH} info :green:DONE 🎉💃✔️ ${OUTPUT}
	@${MH} hbar
	@${MH} info :green:SIZE ${OUTPUT} on ${MCU}
	@${MH} size ${OUTPUT} ${MCU} ${FLASHSZ} ${RAMSZ} ${EESZ}

ifeq (${NODEP},0)
include ${DEPS}
endif

${OUTPUT}: ${OBJECTS}
	@${MH} info LINK $@ ← $^
	@${CXX} ${CFLAGS} $^ -o $@

%.disas: %.elf
	@${MH} info OBJDUMP $@
	@${OBJDUMP} -SC $< > $@

%.o: %.cpp
	@${MH} run CXX $< -- ${CXX} ${CXXFLAGS} -c -o $@ $(<:.d=.cpp) ${GCC_FORMAT}

%.o: %.c
	@${MH} run CC $< -- ${CC} ${CFLAGS} -c -o $@ $(<:.d=.c) ${GCC_FORMAT}

clean:
	@${MH} info :cyan:RM ${OUTPUTS} ${OBJECTS} ${DEPS}
	@rm -f ${OUTPUTS} ${OBJECTS} ${DEPS}
