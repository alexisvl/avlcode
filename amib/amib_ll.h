// AMIB low-level driver
// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

// This AMIB low-level implementation runs on AVR-Dx microcontrollers,
// and uses the following peripherals:
//
// 1 USART
// 1 TCB, for bus idle detection
// 1 GPR instance, for fast state tracking
// 1 event channel, for bus idle detection
// 1 optional GPIO to control transceiver silent mode
//
// To use silent mode, a CANbus transceiver capable of full speed
// receive while in Silent is required. It must be guaranteed
// able to transition from Silent to Active in no more than the AMIB
// bus idle time, as the bus idle timer is used to time this transition.
//
// The CPU clock rate must be at least 4 MHz to achieve the AMIB bus clock
// of 500 kbaud.
//
// WARNING on EVSYS channel mapping: the channel MUST be selected according to
// the port bearing TX and RX, as follows:
//
//  Port    Channels
//  PORTA   0 or 1
//  PORTB   0 or 1
//  PORTC   2 or 3
//  PORTD   2 or 3
//  PORTE   4 or 5
//  PORTF   4 or 5
//  PORTG   6 or 7

#ifndef AMIB_LL_H
#define AMIB_LL_H 1

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include "amib_types.h"

// Standard headers
#include <stdbool.h>
#include <inttypes.h>
#include <stddef.h>

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------
// --- PUBLIC CONSTANTS --------------------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

/// Initialize AMIB.
///
/// All peripherals and interrupts used by AMIB will be set up at this point. If
/// a PORTMUX setting is required for the USART, or if the silent mode pin needs
/// INVEN set, this should be configured first.
///
/// Uses stderr.
///
/// @retval AMIB_SUCCESS success
amib_error_t amib_ll_init(void);

/// Send an AMIB message. If there is space in the transmit queue, it will be
/// added to the queue and this function will return. Otherwise, if @a block is
/// true, this function will block until there is space; if not, it will return
/// -EBUSY.
///
/// @param msg - message to send. Correct SOF and checksum are not required.
/// @param block - whether to block
/// @retval AMIB_SUCCESS - success
/// @retval AMIB_NOT_AVAILABLE - queue full and @a block was false
amib_error_t amib_ll_send(amib_message_t const * msg, bool block);

/// Pop am AMIB message from the queue, if there is one. Never blocks.
///
/// This must be called often enough to handle incoming messages, otherwise the
/// queue will fill up.
///
/// @param msg - message buffer to receive into
/// @retval AMIB_SUCCESS - success
/// @retval AMIB_NOT_AVAILABLE - no message to receive
amib_error_t amib_ll_receive(amib_message_t * msg);

/// Pop a set of status flags. Once called, flags are cleared.
amib_flags_t amib_ll_pop_flags(void);

#endif // AMIB_LL_H
