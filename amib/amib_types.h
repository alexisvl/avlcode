/*
 * amib - AMIB types and defines
 * Copyright (C) 2021  Alexis Lockwood
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AMIB_TYPES_H
#define AMIB_TYPES_H 1

#include <inttypes.h>
#include <stdbool.h>

/// AMIB message struct
typedef union {
    struct __attribute__((packed)) {
        uint8_t sof[2];
        union {
            struct __attribute__((packed)) {
                unsigned char dlc : 4;
                unsigned char mti_reserved : 1;
                unsigned char mtf : 3;
            };
            uint8_t mti;
        };
        uint8_t gad;
        uint16_t reg;
        uint8_t data[15];
        uint16_t cksum;
    };
    uint8_t bytes[23];
} amib_message_t;

/// AMIB errors and statuses
typedef enum {
    AMIB_SUCCESS,
    AMIB_NOT_AVAILABLE,
} amib_error_t;

typedef enum {
    /// An incoming message was lost because the queue was full
    AMIB_INCOMING_LOST = 0x01,
} amib_flags_t;

typedef enum {
    AMIB_MTF_REGISTER_UPDATE    = 0,
    AMIB_MTF_REGISTER_QUERY     = 1,
    AMIB_MTF_REGISTER_RESPONSE  = 2,
    AMIB_MTF_EVENT_NOTIFICATION = 4,
} amib_mtf_t;

#define AMIB_BUS_IDLE_TIME_S 143e-6
#define AMIB_MAX_TPA 4095
#define AMIB_SOF 0xAA
#define AMIB_BAUD 500000

/// Number of total checksummed bytes, based on the dlc or mti
#define AMIB_CHECKSUMMED_BYTES(dlc_mti) (((dlc_mti) & 0xFu) + 6u)

#endif // AMIB_TYPES_H
