// AMIB low-level driver for AVR-Dx
// Copyright (C) 2021 Alexis Lockwood
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

// --- DEPENDENCIES ------------------------------------------------------------

// This module
#include "amib_ll.h"

// Supporting modules
#include "fletcher16.h"
#include "ucqueue.h"
#include "avl_queue.hpp"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/atomic.h>

// control flow
//
//  (rx) = independent receive. RXC is not participating in the state machine,
//  rather, it is receiving and enqueuing messages.
//
//                          =======usart=======     timer
//  STATE/event     main    DRE     TXC     RXC     CAPT (timeout)
//
//  READY                                   (rx)
//  outgoing msg    enqueue                 (rx)
//  AWAIT_BUS_IDLE  start idle detector     (rx)
//                                          (rx)
//                                          (rx)    draw random number
//                                          (rx)      > TPA: restart idle detect
//                                          (rx)
//                                          (rx)    draw random number
//  TRANSMIT                                          <= TPA: abort receive, enable DRE
//                          ->sof1
//                          ->sof2
//                          ->mti           <-sof1
//                          ->gad           <-sof2
//  COLLISION               ->reg0          <-mti, detected collision! DRE off, TXC on
//                          ->reg1
//  AWAIT_BUS_IDLE                  adjust TPA, start idle detector and receiver
//                                          (rx)
//                                          (rx)    draw random number
//  TRANSMIT                                          <= TPA: abort receive, enable DRE
//                          ->sof1
//                          ->sof2
//                          ->mti           <-sof1
//                          ->gad           <-sof2
//                          ->reg0          <-mti
//                          ->reg1          <-gad
//                          ->data0         <-reg0
//                              ...         <-reg1
//                          ->dataN         <-data0
//                          ->cksum0            ...
//                          ->cksum1        <-dataN
//                          disable DRE, start idle detector
//                                          <-cksum0
//                                          <-cksum1, deque message (success)
//  READY                                   (rx)
//
// Not shown: at the end of TRANSMIT following "disable DRE, start idle
// detector", if either idle timeout fires or RXC fires with an error rather
// than success, COLLISION sequence is entered again.

// --- CONFIGURATION HEADER CHECKS --------------------------------------------
// See amib_config_example.h for documentation
#include "amib_config.h"

#ifndef AMIB_USART
# error "AMIB_USART must be defined in amib_config.h"
#endif

#ifndef AMIB_USART_DRE_vect
# error "AMIB_USART_DRE_vect must be defined in amib_config.h"
#endif

#ifndef AMIB_USART_TXC_vect
# error "AMIB_USART_TXC_vect must be defined in amib_config.h"
#endif

#ifndef AMIB_USART_RXC_vect
# error "AMIB_USART_RXC_vect must be defined in amib_config.h"
#endif

#ifndef AMIB_TCB
# error "AMIB_TCB must be defined in amib_config.h"
#endif

#ifndef AMIB_TCB_INT_vect
# error "AMIB_TCB_INT_vect must be defined in amib_config.h"
#endif

#ifndef AMIB_GPR
# error "AMIB_GPR must be defined in amib_config.h"
#endif

#ifndef AMIB_EVSYS_CH
# error "AMIB_EVSYS_CH must be defined in amib_config.h"
#endif

// AMIB_SILENT_VPORT is the vport containing the GPIO that will be used
// to enter Silent mode. Leave undefined to not use silent mode.

// AMIB_SILENT_PIN_bp is the pin bit position of the Silent mode GPIO.
// If AMIB_SILENT_VPORT is defined, this must also be defined.
//
// The pin will be driven high to enter Silent mode. If it should be driven
// low, the INVEN (inverted IO) bit should be set prior to AMIB initialization.

// AMIB_TX_RX_VPORT is the vport containing TX and RX pins
// AMIB_TX_PIN_bp is the pin bit position of the TX GPIO.
// AMIB_RX_PIN_bp is the pin bit position of the RX GPIO.

// AMIB_DEBUG_VPORT is an optional vport containing timing debug pins.
//  All _bp defines are also optional.
// AMIB_DEBUG_DRE_bp asserts when DRE is active
// AMIB_DEBUG_TXC_bp asserts when TXC is active
// AMIB_DEBUG_RXC_bp asserts when RXC is active
// AMIB_DEBUG_OVF_bp asserts when OVF is active
// AMIB_DEBUG_CAPT_bp asserts when CAPT is active

// AMIB_TX_QUEUE_LEN is the number of transmit queue entries that should be
// created.
#ifndef AMIB_TX_QUEUE_LEN
# error "AMIB_TX_QUEUE_LEN must be defined in amib_config.h"
#endif

// AMIB_RX_QUEUE_LEN is the number of receive queue entries that should be
// created.
#ifndef AMIB_RX_QUEUE_LEN
# error "AMIB_RX_QUEUE_LEN must be defined in amib_config.h"
#endif

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------

/// Flags used in GPR
typedef enum {
    // One-hot state flags
    AMIB_GPR_AWAIT_BUS_IDLE = 0x01u,
    AMIB_GPR_TRANSMITTING   = 0x02u,
    AMIB_GPR_TX_COMPLETE    = 0x04u,
} amib_gpr_t;

typedef enum {
    AMIB_BYTE_SOF_1,
    AMIB_BYTE_SOF_2,
    AMIB_BYTE_MTI,
    AMIB_BYTE_GAD,
    AMIB_BYTE_REG_LO,
    AMIB_BYTE_REG_HI,
    AMIB_BYTE_DATA_0,
    AMIB_BYTE_DATA_1,
    AMIB_BYTE_DATA_2,
    AMIB_BYTE_DATA_3,
    AMIB_BYTE_DATA_4,
    AMIB_BYTE_DATA_5,
    AMIB_BYTE_DATA_6,
    AMIB_BYTE_DATA_7,
    AMIB_BYTE_DATA_8,
    AMIB_BYTE_DATA_9,
    AMIB_BYTE_DATA_10,
    AMIB_BYTE_DATA_11,
    AMIB_BYTE_DATA_12,
    AMIB_BYTE_DATA_13,
    AMIB_BYTE_DATA_14,
    AMIB_BYTE_CKSUM_LO,
    AMIB_BYTE_CKSUM_HI,
    AMIB_BYTE_FINISHED
} amib_byte_idx_t;

typedef enum {
    AMIB_DBG_EXIT_ISR   = 0x00,
    AMIB_DBG_ENTER_DRE  = 0x01,
    AMIB_DBG_ENTER_TXC  = 0x02,
    AMIB_DBG_ENTER_RXC  = 0x03,
    AMIB_DBG_ENTER_OVF  = 0x04,
    AMIB_DBG_ENTER_CAPT = 0x05,

    AMIB_DBG_SEND_WHILE_STOPPED = 0x10,
    AMIB_DBG_SEND_WHILE_RUNNING = 0x11,
    AMIB_DBG_START_IDLE_DETECT  = 0x12,
    AMIB_DBG_STOP_IDLE_DETECT   = 0x13,

    AMIB_DBG_RXC_FERR_PERR      = 0x14,
    AMIB_DBG_RXC_BUFOVF         = 0x15,
    AMIB_DBG_RXC_CKSUM_FAIL     = 0x16,
    AMIB_DBG_RXC_MATCH_FAIL     = 0x17,
    AMIB_DBG_RXC_FINISH_NO_NEW  = 0x18,
    AMIB_DBG_RXC_FINISH_ANOTHER = 0x19,
    AMIB_DBG_RXC_FINISH_RX      = 0x1A,

    AMIB_DBG_ADJUST_TPA_UP      = 0x20,
    AMIB_DBG_ADJUST_TPA_DOWN    = 0x21,
    AMIB_DBG_TPA_PASS           = 0x22,
    AMIB_DBG_TPA_FAIL           = 0x23,
} amib_debug_message_t;

// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------

static void _adjust_tpa_up(void);
static void _adjust_tpa_down(void);
static bool _check_tpa(void);

/// Get the byte offset within amib_message_t for a given byte index,
/// taking the DLC into account.
///
/// @param idx - index
/// @param msg - message containing bytes
static uint8_t _offset_for_idx(
    amib_byte_idx_t idx,
    amib_message_t const * msg
);

/// Strobe out a debug message.
static inline void _debug_message(amib_debug_message_t msg);
static void _start_idle_detect(void);

// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------

static volatile uint8_t _amib_flags = 0;
static volatile uint8_t _amib_tx_byte_idx = AMIB_BYTE_SOF_1;
static volatile uint8_t _amib_rx_byte_idx = AMIB_BYTE_SOF_1;

static avl::queue<amib_message_t, AMIB_TX_QUEUE_LEN> _tx_queue;
static avl::queue<amib_message_t, AMIB_RX_QUEUE_LEN> _rx_queue;
static amib_message_t const * _outbox = NULL;

static uint16_t _amib_tpa = AMIB_MAX_TPA;

// Accumulator for rapid, crappy pseudorandom number generation. We just
// fletcher16 all bytes ever sent or received by the USART and use that.
// Seeded in amib_ll_init() by device-specific data.
static uint16_t _amib_prng;

// --- PUBLIC FUNCTIONS --------------------------------------------------------

static_assert(
    F_CPU >= 4000000,
    "F_CPU must be at least 4 MHz for proper AMIB operation"
);

amib_error_t amib_ll_init(void)
{
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
        // Map the RX pin to the timer capture event
        static_assert(AMIB_EVSYS_CH >= 0 && AMIB_EVSYS_CH <= 9,
                "invalid AMIB_EVSYS_CH (0 to 9)"
        );
        if (&AMIB_TX_RX_VPORT == &VPORTA || &AMIB_TX_RX_VPORT == &VPORTC
                || &AMIB_TX_RX_VPORT == &VPORTE || &AMIB_TX_RX_VPORT == &VPORTG
        ) {
            (&EVSYS.CHANNEL0)[AMIB_EVSYS_CH] = 0x40 | AMIB_RX_PIN_bp;
        } else {
            (&EVSYS.CHANNEL0)[AMIB_EVSYS_CH] = 0x48 | AMIB_RX_PIN_bp;
        }

        if (&AMIB_TCB == &TCB0) {
            EVSYS.USERTCB0CAPT = AMIB_EVSYS_CH + 1;
        } else if (&AMIB_TCB == &TCB1) {
            EVSYS.USERTCB1CAPT = AMIB_EVSYS_CH + 1;
        } else if (&AMIB_TCB == &TCB2) {
            EVSYS.USERTCB1CAPT = AMIB_EVSYS_CH + 1;
        } else if (&AMIB_TCB == &TCB3) {
            EVSYS.USERTCB1CAPT = AMIB_EVSYS_CH + 1;
        } else if (&AMIB_TCB == &TCB4) {
            EVSYS.USERTCB1CAPT = AMIB_EVSYS_CH + 1;
        }

        // Timer configuration
        //
        // The timer is used in "Input Capture on Event" mode to detect bus
        // idle. To check for idle, the timer is first initialized to a count
        // of MAX - IDLE_DURATION, so that it will overflow after an idle
        // period. It is then started. A falling edge on the bus will trip
        // a CAPT interrupt, which clears the bus-idle flag and resets the
        // timer. If it makes it all the way to MAX, an OVF interrupt is
        // triggered and we declare idle.
        //
        // Yes, this could miss the bus being stuck low for the entire duration.
        // To guard against this, we check both the actual bus state and the
        // CAPT flag in the OVF interrupt handler. This detects both "bus was
        // low the entire time" and "bus was low, then went high after the OVF
        // interrupt already fired".

        AMIB_TCB.CTRLB = 0
            | TCB_CNTMODE_INT_gc
            ;

        AMIB_TCB.EVCTRL = 0
            | TCB_EDGE_bm
            | TCB_CAPTEI_bm
            ;

        AMIB_TCB.INTCTRL = 0;

        AMIB_TCB.CTRLA = 0
            | TCB_RUNSTDBY_bm
            ;

        for (int i = 0; i < 16; i += 1) {
            _amib_prng = fletcher16(_amib_prng, (&SIGROW.SERNUM0)[i]);
        }

        AMIB_GPR = 0;

        AMIB_USART
            << +avl::usart_baud::baud<AMIB_BAUD>()
            << +avl::usart_ctrla::rxcie::yes
            << +avl::usart_ctrlb::rxen::yes + avl::usart_ctrlb::txen::yes
                + avl::usart_ctrlb::rxmode::normal
            << +avl::usart_ctrlc::cmode::async + avl::usart_ctrlc::parity::even
                + avl::usart_ctrlc::stopbits::one + avl::usart_ctrlc::chsize::eight;
    }

    fprintf_P(
        stderr, PSTR("amibll: success, prng seed = %04X\n"),
        _amib_prng
    );

    return AMIB_SUCCESS;
}

amib_error_t amib_ll_send(amib_message_t const * msg, bool block)
{
    do {
        auto rdest = _tx_queue.emplace_ptr();

        if (rdest.is_ok()) {
            amib_message_t * dest = rdest.ok();
            memcpy(dest, msg, sizeof(amib_message_t));
            dest->sof[0] = AMIB_SOF;
            dest->sof[1] = AMIB_SOF;
            for (size_t i = 0; i < AMIB_CHECKSUMMED_BYTES(msg->dlc); i += 1) {
                dest->cksum = fletcher16(dest->cksum, msg->bytes[i]);
            }

            ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
                _tx_queue.push_emplaced();
                if (!AMIB_GPR) {
                    AMIB_GPR |= AMIB_GPR_AWAIT_BUS_IDLE;
#ifdef AMIB_SILENT_PIN_bp
                    AMIB_SILENT_VPORT.OUT |= (1 << AMIB_SILENT_PIN_bp);
#endif
                    _debug_message(AMIB_DBG_SEND_WHILE_STOPPED);
                    _start_idle_detect();
                } else {
                    // Otherwise, an ISR is about to fire; it'll take care of
                    // transmitting.
                    _debug_message(AMIB_DBG_SEND_WHILE_RUNNING);
                }
            }
            return AMIB_SUCCESS;
        }
    } while (block);

    return AMIB_NOT_AVAILABLE;
}

amib_error_t amib_ll_receive(amib_message_t * msg)
{
    if (_rx_queue.pop(msg) == avl::error_t::OK) {
        return AMIB_SUCCESS;
    } else {
        return AMIB_NOT_AVAILABLE;
    }
}

amib_flags_t amib_ll_pop_flags(void)
{
    uint8_t flags;

    ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
        flags = _amib_flags;
        _amib_flags = 0;
    }

    return (amib_flags_t) flags;
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------

static void _start_idle_detect(void)
{
    _debug_message(AMIB_DBG_START_IDLE_DETECT);
    AMIB_TCB.CTRLA = 0;
    AMIB_TCB.CNT = 0xFFFFu - (F_CPU * AMIB_BUS_IDLE_TIME_S + 0.5);
    AMIB_TCB.INTFLAGS = TCB_CAPT_bm | TCB_OVF_bm;
    AMIB_TCB.INTCTRL = TCB_CAPT_bm | TCB_OVF_bm;
    AMIB_TCB.CTRLA = 0
            | TCB_ENABLE_bm
            | TCB_RUNSTDBY_bm
            ;
}

static void _stop_idle_detect(void)
{
    _debug_message(AMIB_DBG_STOP_IDLE_DETECT);
    AMIB_TCB.INTCTRL = 0;
    AMIB_TCB.CTRLA = 0;
}

ISR(AMIB_USART_DRE_vect)
{
    _debug_message(AMIB_DBG_ENTER_DRE);

    uint8_t const tx_byte_idx = _amib_tx_byte_idx;

    uint8_t const next_byte = _outbox->bytes[_offset_for_idx((amib_byte_idx_t) tx_byte_idx, _outbox)];

    AMIB_USART.write(next_byte);

    if (tx_byte_idx + 1 >= AMIB_BYTE_FINISHED) {
        _amib_tx_byte_idx = AMIB_BYTE_SOF_1;
        AMIB_USART << avl::usart_ctrla::dreie::no;
        AMIB_GPR |= AMIB_GPR_TX_COMPLETE;
        _start_idle_detect();
    } else if (tx_byte_idx == AMIB_BYTE_DATA_0 + _outbox->dlc - 1) {
        // end of the data, skip the rest of the data states
        _amib_tx_byte_idx = AMIB_BYTE_CKSUM_LO;
    } else {
        _amib_tx_byte_idx = tx_byte_idx + 1;
    }

    _debug_message(AMIB_DBG_EXIT_ISR);
}

ISR(AMIB_USART_TXC_vect)
{
    _debug_message(AMIB_DBG_ENTER_TXC);

    // alright, the transmitter is done causing collisions...
    AMIB_GPR = AMIB_GPR_AWAIT_BUS_IDLE;
    AMIB_USART << avl::usart_ctrla::txcie::no;
    _amib_tx_byte_idx = AMIB_BYTE_SOF_1;
    _adjust_tpa_down();
    _start_idle_detect();

    _debug_message(AMIB_DBG_EXIT_ISR);
}

ISR(AMIB_USART_RXC_vect)
{
    _debug_message(AMIB_DBG_ENTER_RXC);

    auto rxdata = AMIB_USART.read();
    uint8_t const data = rxdata.data & 0xFF;

    static uint16_t rx_checksum = 0;
    static amib_message_t rx_buf;
    uint8_t const rx_byte_idx = _amib_rx_byte_idx;

    if (rxdata.ferr || rxdata.perr) {
        _debug_message(AMIB_DBG_RXC_FERR_PERR);
        goto msgfail;
    }

    if (rxdata.bufovf) {
        _debug_message(AMIB_DBG_RXC_BUFOVF);
        _amib_flags |= AMIB_INCOMING_LOST;
        goto msgfail;
    }

    rx_buf.bytes[_offset_for_idx((amib_byte_idx_t) rx_byte_idx, &rx_buf)] = data;

    if (rx_byte_idx <= AMIB_BYTE_SOF_2 && data != AMIB_SOF) {
        goto msgfail;
    }

    if (rx_byte_idx <= AMIB_CHECKSUMMED_BYTES(rx_buf.mti)) {
        rx_checksum = fletcher16(
            rx_byte_idx == AMIB_BYTE_SOF_1 ? 0 : rx_checksum,
            data
        );
    }

    if (AMIB_GPR
            && data != _outbox->bytes[_offset_for_idx((amib_byte_idx_t) rx_byte_idx, _outbox)]
    ) {
        _debug_message(AMIB_DBG_RXC_MATCH_FAIL);
        goto msgfail;
    }

    if (rx_byte_idx < AMIB_CHECKSUMMED_BYTES(rx_buf.mti) - 1) {
        _amib_rx_byte_idx = rx_byte_idx + 1;
    } else if (rx_byte_idx == AMIB_CHECKSUMMED_BYTES(rx_buf.mti - 1)) {
        _amib_rx_byte_idx = AMIB_BYTE_CKSUM_LO;
    } else if (rx_byte_idx == AMIB_BYTE_CKSUM_HI) {
        if (rx_checksum != rx_buf.cksum) {
            _debug_message(AMIB_DBG_RXC_BUFOVF);
            goto msgfail;
        }
    }

    if (rx_byte_idx == AMIB_BYTE_CKSUM_HI) {
        // success!
        if (AMIB_GPR) {
            _tx_queue.pop();
            _adjust_tpa_up();

            _outbox = _tx_queue.read().ok_or(nullptr);
            if (_outbox) {
                _debug_message(AMIB_DBG_RXC_FINISH_ANOTHER);
                AMIB_GPR = AMIB_GPR_AWAIT_BUS_IDLE;
                _start_idle_detect();
            } else {
                _debug_message(AMIB_DBG_RXC_FINISH_NO_NEW);
                AMIB_GPR = 0;
#ifdef AMIB_SILENT_PIN_bp
                AMIB_SILENT_VPORT.OUT &= ~(1 << AMIB_SILENT_PIN_bp);
#endif
                _stop_idle_detect();
            }
        } else {
            _debug_message(AMIB_DBG_RXC_FINISH_RX);
            amib_message_t * msg = _rx_queue.emplace_ptr().ok_or(nullptr);
            if (msg) {
                memcpy(msg, &rx_buf, sizeof(*msg));
                _rx_queue.push_emplaced();
            } else {
                _amib_flags |= AMIB_INCOMING_LOST;
            }
            _amib_rx_byte_idx = AMIB_BYTE_SOF_1;
        }
    }

    _debug_message(AMIB_DBG_EXIT_ISR);
    return;

msgfail:
    _amib_rx_byte_idx = AMIB_BYTE_SOF_1;
    if (AMIB_GPR) {
        AMIB_USART
            << avl::usart_ctrla::txcie::yes
             + avl::usart_ctrla::dreie::no;
    }

    _debug_message(AMIB_DBG_EXIT_ISR);
}

ISR(AMIB_TCB_INT_vect)
{
    if (AMIB_TCB.INTFLAGS & TCB_OVF_bm) {
        _debug_message(AMIB_DBG_ENTER_OVF);
        AMIB_TCB.INTFLAGS = TCB_OVF_bm;

        // We think the bus is idle, but check for
        //  - bus was low all the time
        //  - bus was low all the time, then just flipped now
        if (!(AMIB_TX_RX_VPORT.IN & (1 << AMIB_RX_PIN_bp))
                || (AMIB_TCB.INTFLAGS & TCB_CAPT_bm))
        {
            _start_idle_detect();
            _debug_message(AMIB_DBG_EXIT_ISR);
            return;
        }

        _stop_idle_detect();

        uint8_t const gpr = AMIB_GPR;

        if (!gpr || (gpr & AMIB_GPR_TX_COMPLETE)) {
            // Idle during receive, reset the receiver.
            _amib_rx_byte_idx = AMIB_BYTE_SOF_1;
        } else if (gpr & AMIB_GPR_TRANSMITTING) {
            // Idle while transmitting. Collision!
            AMIB_GPR = AMIB_GPR_AWAIT_BUS_IDLE;
            _amib_tx_byte_idx = AMIB_BYTE_SOF_1;
            AMIB_USART << avl::usart_ctrla::dreie::no;
            _adjust_tpa_down();
            _start_idle_detect();
        } else if (gpr & AMIB_GPR_AWAIT_BUS_IDLE) {
            // Bus is idle, start transmitting
            if (_check_tpa()) {
                AMIB_GPR |= AMIB_GPR_TRANSMITTING;
                AMIB_USART << avl::usart_ctrla::dreie::yes;
            } else {
                _start_idle_detect();
            }
        }

        _debug_message(AMIB_DBG_EXIT_ISR);
    }

    if (AMIB_TCB.INTFLAGS & TCB_CAPT_bm) {
        _debug_message(AMIB_DBG_ENTER_CAPT);
        AMIB_TCB.INTFLAGS = TCB_CAPT_bm;

        // The bus wiggled!
        _start_idle_detect();

        _debug_message(AMIB_DBG_EXIT_ISR);
    }
}

static void _adjust_tpa_up(void)
{
    _debug_message(AMIB_DBG_ADJUST_TPA_UP);
    uint16_t const new_tpa = _amib_tpa + (_amib_tpa + 31) / 32;
    if (new_tpa > AMIB_MAX_TPA) {
        _amib_tpa = AMIB_MAX_TPA;
    } else {
        _amib_tpa = new_tpa;
    }
}

static void _adjust_tpa_down(void)
{
    _debug_message(AMIB_DBG_ADJUST_TPA_DOWN);
    uint16_t const new_tpa = _amib_tpa - _amib_tpa / 4;
    _amib_tpa = new_tpa;
}

static bool _check_tpa(void)
{
    static uint8_t wobble = 1;

    uint16_t const rn = _amib_prng % AMIB_MAX_TPA;

    _amib_prng = fletcher16(_amib_prng, _amib_tpa ^ wobble);
    // cycle length = 256
    wobble ^= (wobble << 1);
    wobble ^= (wobble >> 3);
    wobble ^= (wobble << 5);

    if (rn <= _amib_tpa) {
        _debug_message(AMIB_DBG_TPA_PASS);
        return true;
    } else {
        _debug_message(AMIB_DBG_TPA_FAIL);
        return false;
    }
}

static uint8_t _offset_for_idx(
    amib_byte_idx_t idx,
    amib_message_t const * msg
)
{
    uint8_t const dlc = msg->mti & 0xF;
    uint8_t offset;
    if (idx >= AMIB_CHECKSUMMED_BYTES(dlc)) {
        offset = idx + (15 - msg->dlc);
    } else {
        offset = idx;
    }

    if (offset >= sizeof(*msg)) {
        return sizeof(*msg) - 1;
    } else {
        return offset;
    }
}

static inline void _debug_message(amib_debug_message_t msg)
{
#ifdef AMIB_DEBUG_PORT
    AMIB_DEBUG_PORT  = msg & 0x7Fu;
    AMIB_DEBUG_PORT += 0x80u;
#endif
}
