/*
 * ucqueue - simple fifo for microcontrollers
 * Copyright (C) 2021  Alexis Lockwood
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ucqueue.h"
#include <string.h>

uint8_t ucqueue_weight(ucqueue_t * queue)
{
    uint8_t const head = queue->head, tail = queue->tail;

    if (head >= tail) {
        return head - tail;
    } else {
        return (queue->length - tail) + head;
    }
}

void const * ucqueue_read(ucqueue_t * queue)
{
    __sync_synchronize();
    if (!queue) {
        return NULL;
    } else if (queue->head == queue->tail) {
        return NULL;
    } else {
        size_t const offset = queue->tail * queue->element_stride;
        uintptr_t const base = (uintptr_t) queue->data;
        uintptr_t const location = base + offset;
        return (void const *) location;
    }
}

bool ucqueue_pop(ucqueue_t * queue, void * dest)
{
    void const * item = ucqueue_read(queue);

    if (item) {
        if (dest) {
            memcpy(dest, item, queue->element_stride);
        }
        uint8_t new_tail = queue->tail + 1;
        if (new_tail >= queue->length) {
            new_tail = 0;
        }
        queue->tail = new_tail;
        __sync_synchronize();
        return true;
    } else {
        return false;
    }
}

void * ucqueue_emplace_ptr(ucqueue_t * queue)
{
    __sync_synchronize();
    if (!queue) {
        return NULL;
    } else if (queue->head - queue->tail == queue->length) {
        return NULL;
    } else {
        size_t const offset = queue->head * queue->element_stride;
        uintptr_t const base = (uintptr_t) queue->data;
        uintptr_t const location = base + offset;
        return (void *) location;
    }
}

bool ucqueue_push_emplaced(ucqueue_t * queue)
{
    void * item = ucqueue_emplace_ptr(queue);

    if (item) {
        uint8_t new_head = queue->head + 1;
        if (new_head >= queue->length) {
            new_head = 0;
        }
        queue->head = new_head;
        __sync_synchronize();
        return true;
    } else {
        return false;
    }
}
