#ifndef AMIB_CONFIG_H
#define AMIB_CONFIG_H 1

#include "hardware.hpp"

/// Length of transmit and receive queues in message count. Data required for
/// each queue is (4 + 23n) bytes.
#define AMIB_TX_QUEUE_LEN   4
#define AMIB_RX_QUEUE_LEN   6

/// AMIB_USART and associated defines should be set to the USART connected to
/// the AMIB transceiver. The USART will be fully configured by amib_ll_init();
/// the PORTMUX and IO settings should be configured by the user.
///
/// VPORT and pin positions are required for bus idle detection.
#define AMIB_USART          amib_usart2
#define AMIB_USART_DRE_vect USART2_DRE_vect
#define AMIB_USART_TXC_vect USART2_TXC_vect
#define AMIB_USART_RXC_vect USART2_RXC_vect
#define AMIB_TX_RX_VPORT    VPORTF
#define AMIB_TX_PIN_bp      4
#define AMIB_RX_PIN_bp      5

/// AMIB_TCB should be set to a TCB instance that can be used for bus idle
/// detection. No pin IOs are required.
#define AMIB_TCB            TCB0
#define AMIB_TCB_INT_vect   TCB0_INT_vect

/// AMIB_EVSYS_CH should be set to the number of an event channel that can be
/// used for bus idle detection.
#define AMIB_EVSYS_CH       0

/// AMIB_GPR should be set to a GPR that can be used for state tracking.
#define AMIB_GPR            GPR_GPR0

/// If the AMIB transceiver has a "silent" low-power non-transmitting mode,
/// these defines allow it to be controlled. The pin is assumed active-high
/// to enter silent mode.
///
/// If no silent mode is present, omit these defines. If the pin is active-low,
/// set INVEN for that pin manually prior to init.
///
/// This driver requires silent mode to have the following properties:
///     - Full-speed receive during silent mode.
///     - Transition in and out of silent mode in at least 100us.
#define AMIB_SILENT_VPORT   VPORTF
#define AMIB_SILENT_PIN_bp  3

/// For debugging AMIB timing, a debug port and pins can be defined. The entire
/// port is used; a 7-bit status word is clocked out using the 8th bit as a
/// rising strobe. See amib_ll.c for debug message definitions.
///
/// Do not define if not wanted.
#define AMIB_DEBUG_PORT    amib_debug_port


#endif // AMIB_CONFIG_H
