/*
 * amib - register-level interface
 * Copyright (C) 2021  Alexis Lockwood
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AMIB_H
#define AMIB_H 1

#include "amib_types.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stddef.h>

#if !defined(__AVR__) && !defined(__flash)
# define __flash
#endif

/// Register table entry. The user should define an array of these in __flash.
/// They MUST be stored in ascending order, first by GAD, then by register ID.
typedef struct {
    uint8_t gad;
    uint16_t reg;

    /// Maximum length of the register.  Data received longer than this will be
    /// truncated.
    uint8_t reg_max_len : 4;

    /// Whether we are the primary originator for this register. If set, we will
    /// answer Register Query Packets requesting it.
    bool primary_originator : 1;

    /// Whether the Update Callback should be called for this register.
    bool update_callback : 1;

    /// Whether the Event Callback should be called for this register on
    /// response to events.
    bool event_callback : 1;
} amib_reg_def_t;

/// Compute the amount of space required in RAM to track the given registers.
/// This space may be allocated dynamically (e.g. by malloc, or by alloca
/// inside main), or it may be precomputed (e.g. by first allocating dynamically
/// and checking the space needed).
///
/// Note that no attempt is currently made to consider alignment, under the
/// assumption that the target is an 8-bit platform with no such notion.
///
/// The amount of space required is printed to stderr.
///
/// @param reg_defs - register definitions
/// @param n_reg_defs - number of register definitions
/// @return space required in bytes
size_t amib_space_required(
    amib_reg_def_t const __flash * reg_defs,
    size_t n_reg_defs
);

/// Initialize AMIB. This does NOT initialize the low-level interface, which
/// must be done separately (by amib_init_ll()). There is no sequencing
/// requirement between inits, only that both be called before amib_poll(),
/// amib_set_register*(), amib_get_register*(), amib_request_register(), or
/// amib_send_event().
///
/// @param reg_defs - register definitions
/// @param n_reg_defs - number of register definitions
/// @param arena - space in RAM at least as long as amib_space_required()
void amib_init(
    amib_reg_def_t const __flash * reg_defs,
    size_t n_reg_defs,
    uint8_t * arena
);

/// Poll the low-level interface for updates to registers. This must be called
/// often enough to keep the receive buffer from overflowing. It may block, if
/// the transmit buffer is full.
///
/// Callbacks will be called from this function.
void amib_poll(void);

/// Set the value of a register, broadcasting the change.
///
/// @param gad - group address
/// @param reg - register ID
/// @param data - data content. May be NULL if @a len is 0.
/// @param len - length of @a data
void amib_set_register(uint8_t gad, uint16_t reg, void const * data, size_t len);

/// Get the value of a register.
///
/// @param gad - group address
/// @param reg - register ID
/// @param data - data content buffer
/// @param len - length of @a data. Should be set to the initial length of the
///     buffer (any data exceeding this will be truncated); it will be modified
///     to the true number of bytes written.
/// @retval true - real register value returned
/// @retval false - default value returned; register value has not been received
bool amib_get_register(uint8_t gad, uint16_t reg, void * data, size_t * len);

/// Send a request for the value of a register. The register need not be in the
/// local database.
///
/// @param gad - group address
/// @param reg - register ID
void amib_request_register(uint8_t gad, uint16_t reg);

/// Send an event. The register need not be in the local database, but if it is
/// not, multi-updates cannot be sent.
///
/// @param gad - group address
/// @param reg - register ID
/// @param multi - if true, this is a redundant broadcast of the previous event
///     (to be serviced only once per receiver).
void amib_send_event(uint8_t gad, uint16_t reg, bool multi);



/// Set the value of a register as a uint8_t.
///
/// @param gad - group address
/// @param reg - register ID
/// @param value - value
static inline void amib_set_register_u8(uint8_t gad, uint16_t reg, uint8_t value)
{
    amib_set_register(gad, reg, &value, sizeof(value));
}

/// Set the value of a register as a uint16_t, little endian.
///
/// @param gad - group address
/// @param reg - register ID
/// @param value - value
static inline void amib_set_register_u16(uint8_t gad, uint16_t reg, uint16_t value)
{
    amib_set_register(gad, reg, &value, sizeof(value));
}

/// Set the value of a register as an int16_t, little endian.
///
/// @param gad - group address
/// @param reg - register ID
/// @param value - value
static inline void amib_set_register_s16(uint8_t gad, uint16_t reg, int16_t value)
{
    uint16_t const value_u16 = value;
    amib_set_register(gad, reg, &value_u16, sizeof(value_u16));
}

/// Get the value of a register as a uint8_t.
///
/// @param gad - group address
/// @param reg - register ID
/// @return value
static inline uint8_t amib_get_register_u8(uint8_t gad, uint16_t reg)
{
    uint8_t buf;
    size_t size = 1;
    if (amib_get_register(gad, reg, &buf, &size) && size) {
        return buf;
    } else {
        return 0;
    }
}

/// Get the value of a register as a uint16_t.
///
/// @param gad - group address
/// @param reg - register ID
/// @return value
static inline uint16_t amib_get_register_u16(uint8_t gad, uint16_t reg)
{
    uint8_t buf[2];
    size_t size = 2;
    if (amib_get_register(gad, reg, &buf[0], &size) && size == 2) {
        uint16_t const n = buf[0] | ((uint16_t)(buf[1]) << 8);
        return n;
    } else {
        return 0;
    }
}

/// Get the value of a register as an int16_t.
///
/// @param gad - group address
/// @param reg - register ID
/// @return value
static inline uint16_t amib_get_register_s16(uint8_t gad, uint16_t reg)
{
    uint8_t buf[2];
    size_t size = 2;
    if (amib_get_register(gad, reg, &buf[0], &size) && size == 2) {
        uint16_t const n_u = buf[0] | ((uint16_t)(buf[1]) << 8);
        if (n_u > INT16_MAX) {
            return (int16_t)(n_u + INT16_MIN) - (UINT16_MAX + INT16_MIN + 1);
        } else {
            return (int16_t) n_u;
        }
    } else {
        return 0;
    }
}

#endif // AMIB_H
