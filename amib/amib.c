/*
 * amib - register-level interface
 * Copyright (C) 2021  Alexis Lockwood
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <avr/pgmspace.h>
#include "amib.h"
#include "amib_ll.h"

/// Struct containing info on a register other than its data. An array of these
/// of equal length to the register definitions list is stored as the first
/// thing in the arena, followed by the raw data space.
typedef struct {
    /// Offset into arena of first byte of data. Undefined if reg_max_len is 0.
    size_t data0_offset;
    /// Actual length of data
    uint8_t len_used : 4;
    /// Whether an update for this register has been seen
    bool update_received : 1;
} amib_reg_handle_t;

/// Storage arena, passed to amib_init().
static uint8_t * _arena;
/// Number of registers defined
static size_t _n_regs;
/// Register definitions
static const __flash amib_reg_def_t * _reg_defs;

/// Find a register by gad and reg.
///
/// @return index of register in arrays
/// @retval (size_t)-1 if not found
static size_t _find_reg(uint8_t gad, uint16_t reg);

/// Return the register handle for the register index.
///
/// @retval NULL if index is invalid
static amib_reg_handle_t * _get_reg_handle(size_t index);

/// Return the data pointer for the register index.
///
/// @retval NULL if index is invalid
static uint8_t * _get_reg_data(size_t index);

#ifndef MIN
#define MIN(x, y) ((x) < (y) ? (x) : (y))
#endif

size_t amib_space_required(
    amib_reg_def_t const __flash * reg_defs,
    size_t n_reg_defs
) {
    size_t total_data = 0;

    for (size_t i = 0; i < n_reg_defs; i += 1) {
        total_data += reg_defs[i].reg_max_len;
    }

    size_t const space = total_data + n_reg_defs * sizeof(amib_reg_handle_t);

    fprintf_P(
        stderr,
        PSTR("amib: space required for arena: %u\n"),
        (unsigned) space
    );

    return space;
}

void amib_init(
    amib_reg_def_t const __flash * reg_defs,
    size_t n_reg_defs,
    uint8_t * arena
) {
    _reg_defs = reg_defs;
    _n_regs = n_reg_defs;
    _arena = arena;

    size_t data0_offset = n_reg_defs * sizeof(amib_reg_handle_t);

    for (size_t i = 0; i < n_reg_defs; i += 1) {
        amib_reg_handle_t * handle = _get_reg_handle(i);
        handle->data0_offset = data0_offset;
        handle->len_used = 0;
        handle->update_received = false;
        data0_offset += _reg_defs[i].reg_max_len;
    }

    fprintf_P(stderr, PSTR("amib: initialized\n"));
}

static size_t _find_reg(uint8_t gad, uint16_t reg)
{
    // Binary search
    size_t index_start = 0;
    size_t index_stop = _n_regs - 1;
    size_t index_mid = index_start + (index_stop - index_start) / 2;

    while (index_start <= index_stop) {
        amib_reg_def_t const __flash * reg_def = &_reg_defs[index_mid];

        if (reg_def->gad < gad || (reg_def->gad == gad && reg_def->reg < reg)) {
            index_start = index_mid + 1;
        } else if (reg_def->gad == gad && reg_def->reg == reg) {
            return index_mid;
        } else {
            index_stop = index_mid - 1;
        }

        index_mid = index_start + (index_stop - index_start) / 2;
    }

    return -1;
}

void amib_poll(void)
{
    amib_message_t msg;

    if (amib_ll_receive(&msg) == AMIB_SUCCESS) {
        size_t const index = _find_reg(msg.gad, msg.reg);

        if (index == (size_t) -1) {
            // still do a callback
            return;
        }

        size_t const trunc_len = MIN(_reg_defs[index].reg_max_len, msg.dlc);

        switch (msg.mtf) {
        case AMIB_MTF_REGISTER_UPDATE:
        case AMIB_MTF_REGISTER_RESPONSE:
            if (_reg_defs[index].reg_max_len && msg.dlc) {
                memcpy(
                    &_arena[_get_reg_handle(index)->data0_offset],
                    msg.data,
                    trunc_len
                );
            }
            _get_reg_handle(index)->len_used = trunc_len;
            if (_reg_defs[index].update_callback
                    && msg.mtf == AMIB_MTF_REGISTER_UPDATE
            ) {
                // callback
            }
            break;

        case AMIB_MTF_REGISTER_QUERY:
            if (_reg_defs[index].primary_originator) {
                //_send_query_response(index);
            }
            break;

        case AMIB_MTF_EVENT_NOTIFICATION:
            if (msg.dlc == 1 && _get_reg_handle(index)->len_used == 1) {
                if (msg.data[0] == _get_reg_data(index)[0]) {
                    return;
                }
            }
            if (_reg_defs[index].event_callback) {
                // callback
            }
            break;

        default:
            break;
        }
    }
}

void amib_set_register(uint8_t gad, uint16_t reg, void const * data, size_t len)
{
    size_t const index = _find_reg(gad, reg);
    if (index == (size_t) -1) {
        return;
    }

    size_t const trunc_len = MIN(_reg_defs[index].reg_max_len, len);

    amib_message_t msg = {
        .dlc = trunc_len,
        .mti_reserved = 0,
        .mtf = AMIB_MTF_REGISTER_UPDATE,
        .gad = gad,
        .reg = reg,
    };

    _get_reg_handle(index)->len_used = trunc_len;
    if (trunc_len == 0) {
    } else {
        memcpy(_get_reg_data(index), data, trunc_len);
        memcpy(&msg.data[0], data, trunc_len);
    }

    amib_ll_send(&msg, true);
}

bool amib_get_register(uint8_t gad, uint16_t reg, void * data, size_t * len)
{
    size_t const index = _find_reg(gad, reg);
    if (index == (size_t) -1) {
        *len = 0;
        return false;
    }

    size_t const trunc_len = MIN(_get_reg_handle(index)->len_used, *len);

    if (trunc_len) {
        memcpy(data, _get_reg_data(index), trunc_len);
    }

    *len = trunc_len;
    return _get_reg_handle(index)->update_received;
}

void amib_request_register(uint8_t gad, uint16_t reg)
{
    amib_message_t msg = {
        .dlc = 0,
        .mti_reserved = 0,
        .mtf = AMIB_MTF_REGISTER_QUERY,
        .gad = gad,
        .reg = reg,
    };
    amib_ll_send(&msg, true);
}

static amib_reg_handle_t * _get_reg_handle(size_t index)
{
    if (index >= _n_regs) {
        return NULL;
    } else {
        return (amib_reg_handle_t *) &_arena[index * sizeof(amib_reg_handle_t)];
    }
}

static uint8_t * _get_reg_data(size_t index)
{
    amib_reg_handle_t * const handle = _get_reg_handle(index);

    if (!handle) {
        return NULL;
    } else {
        return &_arena[handle->data0_offset];
    }
}
