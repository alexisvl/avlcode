#include <stdio.h>
#include <string.h>

#include "fletcher16.h"

uint16_t Fletcher16( uint8_t *data, int count )
{
   uint16_t sum1 = 0;
   uint16_t sum2 = 0;
   int index;

   for ( index = 0; index < count; ++index )
   {
      printf("%02X %02X ->(%02X) ", sum1, sum2, data[index]);
      sum1 = (sum1 + data[index]) % 255;
      sum2 = (sum2 + sum1) % 255;
      printf("%02X %02X\n", sum1, sum2);
   }

   return (sum2 << 8) | sum1;
}

int main(int argc, char ** argv)
{
    if (argc != 2) {
        fprintf(stderr, "no\n");
        return 1;
    }

    uint16_t flet1 = Fletcher16((uint8_t *)argv[1], strlen(argv[1]));
    puts("");

    uint16_t flet2 = 0;

    for (size_t i = 0; argv[1][i]; i += 1) {
        flet2 = fletcher16(flet2, (uint8_t) argv[1][i]);
    }

    printf("fletcher16 1 = %04"PRIx16"\n", flet1);
    printf("fletcher16 2 = %04"PRIx16"\n", flet2);
    return 0;
}
