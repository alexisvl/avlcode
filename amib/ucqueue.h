/*
 * ucqueue - simple fifo for microcontrollers
 * Copyright (C) 2021  Alexis Lockwood
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UCQUEUE_H
#define UCQUEUE_H 1

#ifdef __cplusplus
extern "C" {
#endif

#include <inttypes.h>
#include <stdbool.h>
#include <stddef.h>

/// Simple queue storing data in an array. Threadsafe for single producer
/// single consumer ONLY; requires loads and stores to volatile uint8_t
/// to be atomic.
typedef struct {
    uint8_t volatile head;
    uint8_t volatile tail;
    uint8_t element_stride;
    uint8_t length;
    void * data;
} ucqueue_t;

/// Declare a ucqueue.
#define DECLARE_UCQUEUE(name) ucqueue_t name

/// Define a statically allocated ucqueue.
///
/// @param name - name of queue
/// @param type - type of elements
/// @param len - number of elements
#define DEFINE_UCQUEUE(name, type, len) _DEFINE_UCQUEUE(name, , type, len)

/// Define a static, statically allocated ucqueue.
///
/// @param name - name of queue
/// @param type - type of elements
/// @param len - number of elements
#define DEFINE_STATIC_UCQUEUE(name, type, len) \
    _DEFINE_UCQUEUE(name, static, type, len)

/// Return the number of elements currently in the queue
uint8_t ucqueue_weight(ucqueue_t * queue);

/// Return whether the queue is empty
static inline bool ucqueue_empty(ucqueue_t * queue)
{
    return ucqueue_weight(queue) == 0;
}

/// Return whether the queue is full
static inline bool ucqueue_full(ucqueue_t * queue)
{
    return ucqueue_weight(queue) == queue->length;
}

/// Return a pointer to the next element to be read. Does not pop.
///
/// @param queue - queue to pop from
/// @retval NULL - queue is empty
void const * ucqueue_read(ucqueue_t * queue);

/// Pop an item off the queue.
///
/// @param queue - queue to pop from
/// @param dest - buffer to hold item. May be NULL if the item content is not
///     needed.
/// @retval true - an item was popped
/// @retval false - no items were available
bool ucqueue_pop(ucqueue_t * queue, void * dest);

/// Return a writable pointer to construct an item in the queue in-place.
/// Item must be finalized with ucqueue_push_emplaced() before it is available,
/// and two calls to this function with no ucqueue_push_emplaced() between them
/// will return the same pointer.
///
/// @param queue - queue to emplace onto
/// @retval NULL - no space in queue
void * ucqueue_emplace_ptr(ucqueue_t * queue);

/// Make the last emplace_ptr available for read and pop. If
/// ucqueue_emplace_ptr() has not been called, this will result in uninitialized
/// data being pushed.
///
/// @param queue - queue to emplace onto
/// @retval true - success
/// @retval false - no space in queue. This will only happen if
///     ucqueue_emplace_ptr() was not called or returned NULL.
bool ucqueue_push_emplaced(ucqueue_t * queue);

/// (internal) define a statically allocated ucqueue with specified storage
/// class.
///
/// @param name - name of queue
/// @param sc - storage class
/// @param type - type of elements
/// @param len - number of elements
#define _DEFINE_UCQUEUE(name, sc, type, len)    \
    static type name ## __storage[(len)];       \
    sc ucqueue_t name = { 0, 0, sizeof(type), 0, &(name ## __storage)[0] }

#ifdef __cplusplus
}
#endif

#endif // UCQUEUE_H
