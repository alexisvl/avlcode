/*
 * fletcher checksum library
 * Copyright (C) 2021  Alexis Lockwood
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fletcher16.h"
#include <inttypes.h>

#include <stdio.h>

#ifdef __AVR__
static uint8_t add_mod_255(uint8_t a, uint8_t b)
{
    asm(
        "add %0, %2"                "\n\t"
        "adc %0, __zero_reg__"      "\n\t"
        : "=r" (a)
        : "0" (a), "r" (b)
    );
    return a == 255 ? 0 : a;
}
#else
static uint8_t add_mod_255(uint8_t a, uint8_t b)
{
    return ((unsigned) a + b) % 255;
}
#endif

uint16_t fletcher16(uint16_t previous, uint8_t data)
{
    union {
        uint16_t u16;
        struct {
            uint8_t u8_low;
            uint8_t u8_high;
        };
    } flet = { .u16 = previous };

    flet.u8_low = add_mod_255(flet.u8_low, data);
    flet.u8_high = add_mod_255(flet.u8_high, flet.u8_low);

    return flet.u16;
}
