// Copyright (C) 2021 Alexis Lockwood, <alexlockwood@fastmail.com>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

// --- DEPENDENCIES ------------------------------------------------------------

// This module
#include "avl_avrdx_eeprom.hpp"

// Supporting modules
#include <avr/io.h>
#include <avr/wdt.h>

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------

static void _write_byte(uint8_t volatile * addr, uint8_t data);

// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

void avl::avrdx_eeprom_update_block(void const * src, void * dest, size_t n)
{
	uint8_t const * src_u8 = (uint8_t const *) src;
	uint8_t volatile * dest_u8 = (uint8_t volatile *) dest + EEPROM_START;

	for (size_t i = 0; i < n; i += 1)
	{
		_write_byte(&dest_u8[i], src_u8[i]);
	}
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------

static void _write_byte(uint8_t volatile * addr, uint8_t data)
{
	if (*addr != data)
	{
		wdt_reset();
		while (NVMCTRL.STATUS & NVMCTRL_EEBUSY_bm);
		_PROTECTED_WRITE_SPM(NVMCTRL.CTRLA, NVMCTRL_CMD_EEBER_gc);
		*addr = 0xFF;
		_PROTECTED_WRITE_SPM(NVMCTRL.CTRLA, NVMCTRL_CMD_NONE_gc);
		while (NVMCTRL.STATUS & NVMCTRL_EEBUSY_bm);
		_PROTECTED_WRITE_SPM(NVMCTRL.CTRLA, NVMCTRL_CMD_EEWR_gc);
		*addr = data;
		_PROTECTED_WRITE_SPM(NVMCTRL.CTRLA, NVMCTRL_CMD_NONE_gc);
	}
}
