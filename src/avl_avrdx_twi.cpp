// Copyright (C) 2021 Alexis Lockwood, <alexlockwood@fastmail.com>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

// --- DEPENDENCIES ------------------------------------------------------------

// This module
#include "avl_avrdx_twi.hpp"

// Supporting modules

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

using namespace avl;

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

avrdx_twi::avrdx_twi(TWI_t * twi)
	: m_twi(twi)
	, m_sem_sync(nullptr)
	, m_sem_lock(nullptr)
	, m_packet(nullptr)
	, m_out_index(0)
	, m_in_index(0)
	, m_state(state::IDLE)
{
}

error_t avrdx_twi::init(cfg const & config)
{
	m_sem_sync = OSSemCreate(0);
	if (!m_sem_sync)
		return error_t::NOSPC;
	m_sem_lock = OSSemCreate(1);
	if (!m_sem_lock)
		return error_t::NOSPC;

	m_twi->CTRLA = (
		TWI_SDASETUP_4CYC_gc
		| (config.fast_mode_plus ? TWI_FMPEN_bm : 0)
	);

	m_twi->DUALCTRL = 0;

	m_twi->MBAUD = config.baudreg;

	m_twi->MCTRLA = (
		TWI_RIEN_bm
		| TWI_WIEN_bm
		| TWI_ENABLE_bm
	);

	m_twi->MSTATUS = TWI_BUSSTATE_IDLE_gc;

	return error_t::OK;
}

error_t avrdx_twi::txrx(i2c_packet * packet)
{
	if (!packet)
		return error_t::INVAL;

	INT8U oserr = 0;
	OSSemPend(m_sem_lock, packet->timeout, &oserr);

	if (oserr)
		return error_from_ucos(oserr);

	m_packet = packet;

	m_twi->MSTATUS =
		TWI_RIF_bm | TWI_WIF_bm | TWI_BUSERR_bm | TWI_ARBLOST_bm;

	m_state = state::TRANSMITTING;
	m_out_index = 0;
	m_in_index = 0;
	m_twi->MADDR = (m_packet->addr & 0xFE) | (m_packet->n_out ? 0 : 1);

	// now we wait...

	OSSemPend(m_sem_sync, packet->timeout, &oserr);
	m_packet = nullptr;

	if (oserr)
	{
		OSSemPost(m_sem_lock);
		return error_from_ucos(oserr);
	}

	packet->n_out = m_out_index;
	packet->n_in = m_in_index;

	state final_state = m_state;
	m_state = state::IDLE;
	OSSemPost(m_sem_lock);

	switch (final_state)
	{
	case state::DONE:
		return error_t::OK;
	case state::ADDR_NAK:
		return error_t::NODEV;
	case state::ERROR:
	case state::IDLE: // wtf
	case state::TRANSMITTING: // wtf
	case state::RECEIVING: // wtf
	default: // wtf
		return error_t::IO;
	}
}

void avrdx_twi::twim_isr()
{
	uint8_t const stat = m_twi->MSTATUS;
	bool const error = stat & (TWI_BUSERR_bm | TWI_ARBLOST_bm);
	bool const rxack = stat & TWI_RXACK_bm;
	bool done = false;
	i2c_packet * const p = m_packet;
	m_twi->MSTATUS =
		TWI_RIF_bm | TWI_WIF_bm | TWI_BUSERR_bm | TWI_ARBLOST_bm;

	switch (m_state)
	{
	case state::TRANSMITTING: {
		bool const have_data = m_out_index < p->n_out;

		m_twi->MCTRLB = 0;

		if (error)
		{
			m_state = state::ERROR;
			m_twi->MSTATUS = TWI_BUSSTATE_IDLE_gc;
			done = true;
		}
		else if (m_out_index == 0 && rxack)
		{
			// Address was nak'd
			m_state = state::ADDR_NAK;
			done = true;
		}
		else if (have_data && !rxack)
		{
			m_twi->MDATA = p->buffer[m_out_index++];
		}
		else if (p->n_in)
		{
			m_twi->MADDR = p->addr | 1;
			m_state = state::RECEIVING;
		}
		else
		{
			m_state = state::DONE;
			done = true;
		}
		break;
	    }

	case state::RECEIVING: {
		bool const last_byte = m_in_index == p->n_in - 1;

		m_twi->MCTRLB = 0;
		if (!error)
			p->buffer[m_in_index++] = m_twi->MDATA;

		if (error)
		{
			m_state = state::ERROR;
			m_twi->MSTATUS = TWI_BUSSTATE_IDLE_gc;
			done = true;
		}
		else if (rxack)
		{
			m_state = state::DONE;
			done = true;
		}
		else if (last_byte)
		{
			m_twi->MCTRLB = TWI_ACKACT_bm;
			m_state = state::DONE;
			done = true;
		}
		else
		{
			m_twi->MCTRLB |= TWI_MCMD_RECVTRANS_gc;
		}
		break;
	    }

	default:
		// Well, this interrupt shouldn't have fired while idle,
		// but there ain't much we can do about it...
		m_twi->MSTATUS = TWI_BUSSTATE_IDLE_gc;
		break;
	}

	if (done)
	{
		m_twi->MCTRLB |= TWI_MCMD_STOP_gc;
		OSSemPost(m_sem_sync);
	}
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------
