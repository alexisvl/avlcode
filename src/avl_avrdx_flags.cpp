// Copyright (C) 2021 Alexis Lockwood, <alexlockwood@fastmail.com>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

// --- DEPENDENCIES ------------------------------------------------------------

// This module
#include <avl_avrdx_port.hpp>

// Supporting modules

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

using namespace avl;

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PUBLIC CONSTANTS --------------------------------------------------------

constexpr port_pinctrl_t port_pinctrl::reset;
constexpr port_pinctrl_t port_pinctrl::invert::no;
constexpr port_pinctrl_t port_pinctrl::invert::yes;
constexpr port_pinctrl_t port_pinctrl::level::schmitt;
constexpr port_pinctrl_t port_pinctrl::level::ttl;
constexpr port_pinctrl_t port_pinctrl::pullup::no;
constexpr port_pinctrl_t port_pinctrl::pullup::yes;
constexpr port_pinctrl_t port_pinctrl::isc::intdisable;
constexpr port_pinctrl_t port_pinctrl::isc::bothedges;
constexpr port_pinctrl_t port_pinctrl::isc::rising;
constexpr port_pinctrl_t port_pinctrl::isc::falling;
constexpr port_pinctrl_t port_pinctrl::isc::input_disable;
constexpr port_pinctrl_t port_pinctrl::isc::low_level;
constexpr port_dir_t port_dir::reset;
constexpr port_dir_t port_dir::input;
constexpr port_dir_t port_dir::output;
constexpr port_dir_t port_dir::output_0;
constexpr port_dir_t port_dir::output_1;

// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------
// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------
