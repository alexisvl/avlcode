// Copyright (C) 2021 Alexis Lockwood, <alexlockwood@fastmail.com>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

// --- DEPENDENCIES ------------------------------------------------------------

// This module
#include "avl_parsenums.hpp"

// Supporting modules

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

using namespace avl;

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

result_t<uint8_t> avl::parse_hex_nibble(char c)
{
	if (c >= '0' && c <= '9')
		return c - '0';
	else if (c >= 'A' && c <= 'F')
		return c - 'A' + 10;
	else if (c >= 'a' && c <= 'f')
		return c - 'a' + 10;
	else
		return error_t::INVAL;
}

char avl::to_hex_nibble(uint8_t n)
{
	return "0123456789abcdef"[n & 0xF];
}

result_t<uint32_t> avl::parse_int(char const * s, uint8_t base)
{
	if (!s)
		return error_t::INVAL;

	bool const negative = (s[0] == '-');
	if (negative)
		s++;

	if (base == 0 && s[0] == '0')
	{
		char b = s[1];
		s += 2;

		switch (b)
		{
		case 'x':
		case 'X':
			base = 16;
			break;
		case 'o':
		case 'O':
			base = 8;
			break;
		case 'b':
		case 'B':
			base = 2;
			break;
		default:
			base = 10;
			s -= 1;
			break;
		}
	}
	else if (base == 0)
	{
		base = 10;
	}

	if (base > 16)
		return error_t::INVAL;

	char c;
	uint32_t value = 0;
	while ((c = *s++))
	{
		uint8_t cval;
		if (c >= '0' && c <= '9')
			cval = c - '0';
		else if (c >= 'A' && c <= 'F')
			cval = 10 + c - 'A';
		else if (c >= 'a' && c <= 'f')
			cval = 10 + c - 'a';
		else
			return error_t::INVAL;

		if (cval >= base)
			return error_t::INVAL;

		value *= base;
		value += cval;
	}

	if (negative)
		return UINT32_MAX - value + 1;
	else
		return value;
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------
