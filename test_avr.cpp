#include "avl_avrdx.hpp"
#include <stdbool.h>
#include <avr/eeprom.h>

using namespace avl;
bool volatile edge_detected = 0;

pin<PORTA_ADDR, 0> pin_toggle;
pin<PORTB_ADDR, 1> pin_sense;

pin_group<PORTC_ADDR, 0x3F> pins_count;

usart<USART0_ADDR> serial;

int main()
{
    // Toggle PA0 until falling edge on PB1, counting up on PC0-PC5.

    pin_toggle << port_pinctrl::invert::no + port_pinctrl::invert::yes;
    pin_toggle << port_dir::output_0;
    pins_count << port_dir::output_0;
    pin_sense
        << +port_pinctrl::pullup::yes + port_pinctrl::isc::falling
        << port_dir::input;

    serial
        << +usart_ctrla::none
        << usart_ctrlc::cmode::async + usart_ctrlc::parity::even
            + usart_ctrlc::stopbits::one + usart_ctrlc::chsize::eight
        << usart_baud::baud<500000>()
        << +usart_ctrlb::txen::yes + usart_ctrlb::rxmode::normal;

    sei();

    while (!edge_detected) {
        auto rx = serial.read();
        if (!rx.bufovf && !rx.perr && !rx.ferr) {
            while (!serial.dre());
            serial.write(rx.data);
        }
    }
}

ISR(PORTB_PORT_vect) {
    //if (pin_sense.get_and_clear_intflag()) {
        edge_detected = 1;
    //}
}
